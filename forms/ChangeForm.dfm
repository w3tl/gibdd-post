object Form12: TForm12
  Left = 255
  Top = 138
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1088#1077#1077#1089#1090#1088#1072
  ClientHeight = 178
  ClientWidth = 335
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 72
    Top = 16
    Width = 52
    Height = 13
    Caption = #1053#1072#1079#1074#1072#1085#1080#1077':'
  end
  object lbl2: TLabel
    Left = 14
    Top = 40
    Width = 110
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103':'
  end
  object lbl3: TLabel
    Left = 6
    Top = 65
    Width = 118
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1076#1072#1095#1080' '#1085#1072' '#1087#1086#1095#1090#1091':'
  end
  object lbl4: TLabel
    Left = 45
    Top = 91
    Width = 79
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1088#1077#1077#1089#1090#1088#1072':'
  end
  object lbl5: TLabel
    Left = 45
    Top = 115
    Width = 72
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1089#1087#1080#1089#1082#1072':'
  end
  object edt1: TEdit
    Left = 136
    Top = 12
    Width = 185
    Height = 21
    TabOrder = 0
  end
  object dtp1: TDateTimePicker
    Left = 136
    Top = 36
    Width = 105
    Height = 21
    Date = 41751.899698414350000000
    Time = 41751.899698414350000000
    TabOrder = 1
  end
  object dtp2: TDateTimePicker
    Left = 136
    Top = 61
    Width = 105
    Height = 21
    Date = 41751.899698414350000000
    Time = 41751.899698414350000000
    TabOrder = 2
  end
  object btn1: TButton
    Left = 130
    Top = 144
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 3
    OnClick = btn1Click
  end
  object edt2: TEdit
    Left = 136
    Top = 86
    Width = 49
    Height = 21
    TabOrder = 4
  end
  object edt3: TEdit
    Left = 136
    Top = 110
    Width = 49
    Height = 21
    TabOrder = 5
  end
end
