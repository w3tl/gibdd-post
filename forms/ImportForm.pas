unit ImportForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Vars, ComObj, Grids, DataModule,
  Buttons, StrUtils, IniFiles, ShowImport;

type
  TForm2 = class(TForm)
    lbledt1: TLabeledEdit;
    dlgOpen1: TOpenDialog;
    btn2: TButton;
    pb1: TProgressBar;
    StringGrid1: TStringGrid;
    lbledt2: TLabeledEdit;
    stat1: TStatusBar;
    btn1: TBitBtn;
    lbledt4: TLabeledEdit;
    dtp1: TDateTimePicker;
    txt1: TStaticText;
    lbl2: TLabel;
    grp1: TGroupBox;
    lbl3: TLabel;
    edtStartRow: TEdit;
    lbl4: TLabel;
    edtNextNumber: TEdit;
    lbl5: TLabel;
    edtResDate: TEdit;
    btn3: TBitBtn;
    btn4: TBitBtn;
    btn5: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure dtp1Change(Sender: TObject);
    procedure btn5Click(Sender: TObject);
  private
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  xlRowCount: Integer;
  currentFile: Integer;
  Transact: Boolean;
  curRegNumber, posit: Integer;

  union: Integer;
implementation

{$R *.dfm}

procedure TForm2.btn1Click(Sender: TObject);
begin
  if dlgOpen1.Execute then
  begin
    if dlgOpen1.Files.Count > 1 then
      lbledt1.Text := '����� ' + IntToStr(dlgOpen1.Files.Count) + ' ����(��)'
    else
      lbledt1.Text := dlgOpen1.Files[0];
    currentFile := 0;
    
  end;
end;

function getInsertSQL(): string;
var i: Integer;
begin
  Result := 'INSERT INTO FINES (ID, REG_ID, POSIT, ';
  for i := 0 to tableFinesFields.Count - 2 do
    Result := Result + tableFinesFields[i] + ', ';
  Result := Result + tableFinesFields[tableFinesFields.Count - 1] + ')';
end;
function getValuesSQL(): string;
var i: Integer;
begin
  Result := 'VALUES (:ID,:REG_ID,:POSIT,';
  for i := 0 to tableFinesFields.Count - 2 do
    Result := Result + ':' + tableFinesFields[i] + ',';
  Result := Result + ':' + tableFinesFields[tableFinesFields.Count - 1] + ')';
end;

procedure TForm2.btn2Click(Sender: TObject);
var isEmpty: Boolean; emptyError: Integer; temp, temp1, cdate: string;

  Workbook, WorkSheet, URange, FData: OleVariant;
  Rows: Integer;
  StartRow: Integer;
  i, j: Integer;
begin
  for i := 1 to StringGrid1.RowCount - 1 do
  begin
    StringGrid1.Rows[i].Clear;
  end;
  try
    try
      try
        Excel := GetActiveOleObject('Excel.Application');
      except
        Excel := CreateOleObject('Excel.Application');
      end;
      Excel.Visible := False;
      Workbook := Excel.Workbooks.Open(dlgOpen1.Files[currentFile], 0, True);
      lbl2.Caption := '���� ' + IntToStr(currentFile + 1) + ' �� ' + IntToStr(dlgOpen1.Files.Count);
      Excel.DisplayAlerts := False;
    except
      raise Exception.Create('������ �������� �����!' + #10#13 +
              '���������, ��� ���� ���������� � ��� ���� Excel �������.');
    end;
    WorkSheet := Workbook.ActiveSheet;
    try // �������� ����� ��������� ������
      StartRow := StrToInt(edtStartRow.Text);
    except
      raise Exception.Create('��������� ��������� ������!');
    end;
    // �������� ������� ���� �� �������� �����
    lbledt2.Text := StringReplace(ExtractFileName(dlgOpen1.Files[currentFile]),
                              ExtractFileExt(dlgOpen1.Files[currentFile]),'',[]);
    try // �������� ����� ������� � �.�. � �������� �� � Excel
      cdate := Excel.Cells[StartRow, StrToInt(edtResDate.Text)];
      cdate := StringReplace(cdate, '/', '.', [rfReplaceAll]);
      dtp1.Date := StrToDate(cdate);
      dtp1Change(Sender);
    except on E: Exception do
      begin
        if Length(E.Message) = 0 then
          raise Exception.Create('��������� ����� ������� � ����� �������������!')
        else
          raise Exception.Create(E.Message);
      end;
    end;

    URange := WorkSheet.UsedRange;
    if URange.Rows.Count < StartRow then // ������ ����� ��������� ������� ���
      raise Exception.Create('� ������� ����������� ������!' + #10#13 +
                             '�������, ������� ������������ ��������� ������.');
    URange := Excel.Intersect(URange, URange.Offset[StartRow - 1, 0]); // ������ �� ������� ��������� �������
    Rows := URange.Rows.Count;
    StringGrid1.RowCount := Rows + 1;
    try
      FData := URange.Value;
    except
      raise Exception.Create('������ ������ �����! ' + #10#13 +
                 '�������� �������������� ����� Excel ����� ��.');
    end;
    emptyError := 0;
    pb1.Max := Rows;
    pb1.Position := 0;
    for i := 1 to Rows do begin
      isEmpty := False;
      for j := 0 to tableFinesFields.Count - 1 do begin
        try
          temp := FData[i, tableFinesPos[j]];
        except
          temp := '';
        end;
        if Length(temp) = 0 then
          isEmpty := True;
        if tableFinesFields[j] = 'CITY' then
          temp := ChangeCity(temp);
        // ������� � ����� ����� ����, �������� � ��������
        if tableFinesFields[j] = 'ADDRESS' then
        begin
          temp := '' + temp;
          temp1 := string(FData[i, tableFinesPos[j] + 1]); // House
          if Length(temp1) > 0 then
            temp := temp + ', �. ' + temp1;
          //temp := temp + string(FData[i, tableFinesPos[j] + 1]);
          // ��������, ���� �� ������ ������ � Excel
          if Length(FData[i, tableFinesPos[j] + 2]) <> 0 then
          begin
            temp := temp + '-' + string(FData[i, tableFinesPos[j] + 2]);
          end;
          temp1 := string(FData[i, tableFinesPos[j] + 3]); // Room
          if Length(temp1) > 0 then
            temp := temp + ', ��. ' + temp1;
        end;
        StringGrid1.Cells[j, i] := temp;
        pb1.Position := pb1.Position + 1;
      end;
      if isEmpty then emptyError := emptyError + 1;
    end;
    stat1.Panels[1].Text := IntToStr(Rows);
    stat1.Panels[3].Text := IntToStr(emptyError);
    btn3.Enabled := True;
    btn4.Enabled := True;
    VarClear(FData);
  except on E: Exception do
    begin
      MessageDlg(E.Message, mtError, [mbOk], 0);
      VarClear(FData);
      CloseExcel;
      Exit;
    end;
  end;
  Excel.Visible := True;
end;

procedure TForm2.btn3Click(Sender: TObject);
var id, i, j: Integer;
begin
  id := Random(High(Integer)); // ��������� ����� ID
  with DataModule2 do
  begin
    with IBWorkQuery1 do
    begin
      Close;
      SQL.Text := 'INSERT INTO REGISTRIES (ID,CAPTION,CREATE_DATE,POSIT,REG_NUMBER)';
      SQL.Add(' VALUES (:id,:CAPTION,:CREATE_DATE,:POSIT,:REG_NUMBER)');
      ParamByName('id').AsInteger := id;
      try
        ParamByName('CAPTION').AsString := lbledt2.Text;
        ParamByName('CREATE_DATE').AsDate := dtp1.Date;
        ParamByName('POSIT').AsInteger := posit;
        ParamByName('REG_NUMBER').AsInteger := StrToInt(lbledt4.Text);
        ExecSQL;
        Transaction.Commit;
      except
        MessageDlg('������ ����������! ��������� �������� �������� ��� ������.',
                   mtError, [mbOK], 0);
        Exit;
      end;
    end;
    with IBWorkQuery2 do // �������� �������������
    begin
      Close;
      SQL.Clear;
      SQL.Add(getInsertSQL);
      SQL.Add(getValuesSQL);
      for i := 1 to StringGrid1.RowCount - 1 do
      begin
        ParamByName('ID').AsInteger := Random(High(Integer));
        ParamByName('REG_ID').AsInteger := id;
        ParamByName('POSIT').AsInteger := i;
        try
          for j := 0 to tableFinesFields.Count - 1 do
            ParamByName(tableFinesFields[j]).AsString := StringGrid1.Cells[j, i];
          ExecSQL;
        except // ������ ���������� �������������
          MessageDlg('� ������ ' + IntToStr(i) + ' (' + IntToStr(j + xlStartRow - 1) +')' +
                     ', �������' + tableFinesFields[j] +
                     ' ���������� ������������ ��������!', mtError, [mbAbort], 0);
          Transaction.Rollback; // ��������� ���������� � ������������ ���������������
          if union = 0 then // ���� ��� ������ ����� ������ ��� �������������
            with DataModule2.IBWorkQuery1 do
            begin
              Close;
              Transaction.StartTransaction;
              SQL.Text := 'delete from REGISTRIES where ID=:id'; // ������ ����� ������
              ParamByName('id').Value := id;
              ExecSQL;
              Transaction.Commit;
              DataModule2.IBQuery1.Close;
              DataModule2.IBQuery1.Open;
              Exit;
            end;
        end;
      end;
      Transaction.Commit;
      with IBWorkQuery1 do // �������� ��������� ������������� � ������
      begin
        Close;
        SQL.Text := 'UPDATE REGISTRIES SET FINES_COUNT=' +
          '(SELECT COUNT(ID) FROM FINES WHERE REG_ID=:id) ' +
          'WHERE ID=:id';
        ParamByName('id').AsInteger := id;
        ExecSQL;
        Transaction.Commit;
      end;
    end;

    if (curRegNumber = regNumber) then // ������� ����� ������� == ������ ������� � ����������
    begin
      regNumber := regNumber + 1; // ���������� ����� ������� � ����������
      Ini := TIniFile.Create(IniPath); // � �������� � ����� ��������
      Ini.WriteInteger('Import', 'regNumber', regNumber);
      Ini.Free;
      edtNextNumber.Text := IntToStr(regNumber);
    end;
    currentFile := currentFile + 1; // ���������� ������ �����
    if currentFile < dlgOpen1.Files.Count then // ������� ���� �� ���������
    begin
      lbledt1.Text := dlgOpen1.Files[currentFile]; // ������ ��� �������� �����
      btn2Click(Sender); // ������� ������ �� �����
    end
    else
    begin
      MessageDlg('��� ����� ���������!', mtInformation, [mbOK], 0);
      lbledt1.Text := '�������� ����...';
      btn4.Enabled := False;
      btn3.Enabled := False;
    end;
    IBQuery1.Close;
    IBQuery1.Open;
  end;
end;

procedure TForm2.FormCreate(Sender: TObject);
var i, colWidth: Integer;
begin
  StringGrid1.ColCount := tableFinesFields.Count;
  colWidth := Round(StringGrid1.ClientWidth / tableFinesFields.Count);
  for i := 0 to tableFinesFields.Count - 1 do
  begin
    StringGrid1.ColWidths[i] := colWidth;
    StringGrid1.Cells[i, 0] := tableFinesFields[i];
  end;
end;

procedure TForm2.btn4Click(Sender: TObject);
begin
  while currentFile < dlgOpen1.Files.Count do
  begin
    btn3Click(Sender);
  end;
end;

procedure TForm2.dtp1Change(Sender: TObject);
begin

  /////////// ��������, ���� �� ��� ������� � ����� �� ����� ////////////////
  {with DataModule2.IBQuery3 do
  begin
    Close;
    // ������� ��������� ������ � ����� �� ����� �������������
    SQL.Text := 'SELECT FIRST (1) ID,POSIT,REG_NUMBER FROM REGISTRIES ' +
                'WHERE CREATE_DATE=:cdate ORDER BY POSIT DESC';
    ParamByName('cdate').AsDate := dtp1.Date;
    Open;
    union := 0; // �� ���������� (��-���������)
    if RecordCount > 0 then // ����� ������ ����
    begin
      posit := VarToInt(FieldValues['POSIT']); // ������� ��� �� ����� ������
      if chkUnion.Checked then // ������ ����� "���������� �������"
        // ���������, ���������� �� �� �������
        if MessageDlg('������ ������ � ����� ' + DateToStr(dtp1.Date) +
                          '.' + #10#13 + '������� ������ � ����?', mtWarning,
                          [mbYes, mbNo], 0) = mrNo then  // ���� �� ���������,
          posit := posit + 1 // �� ��������� ����� ������
        else  // ������� ����������, �� ��������� ID ������, ���� ����������
          union := VarToInt(FieldValues['ID']); // ���� ID ������ � �������������
      curRegNumber := VarToInt(FieldValues['REG_NUMBER']); // ���� ����� ������ � �������������
    end
    else // ������� � ����� �� ����� ������������� ���
    begin }
      curRegNumber := StrToInt(edtNextNumber.Text);
      posit := 1;
   {end;}
    try
      lbledt4.Text := IntToStr(curRegNumber);
    except
      raise Exception.Create('��������� ��������� ����� �������!');
    end
  {end;}
end;

procedure TForm2.btn5Click(Sender: TObject);
begin
  Form13.Show;
end;

end.
