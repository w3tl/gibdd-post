object Form3: TForm3
  Left = 205
  Top = 251
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1041#1044
  ClientHeight = 244
  ClientWidth = 201
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbledt1: TLabeledEdit
    Left = 11
    Top = 22
    Width = 178
    Height = 21
    EditLabel.Width = 30
    EditLabel.Height = 13
    EditLabel.Caption = #1051#1086#1075#1080#1085
    TabOrder = 0
  end
  object lbledt2: TLabeledEdit
    Left = 11
    Top = 61
    Width = 178
    Height = 21
    EditLabel.Width = 37
    EditLabel.Height = 13
    EditLabel.Caption = #1055#1072#1088#1086#1083#1100
    TabOrder = 1
  end
  object lbledt3: TLabeledEdit
    Left = 11
    Top = 100
    Width = 178
    Height = 21
    EditLabel.Width = 56
    EditLabel.Height = 13
    EditLabel.Caption = #1050#1086#1076#1080#1088#1086#1074#1082#1072
    TabOrder = 2
  end
  object btn1: TButton
    Left = 166
    Top = 175
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 3
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 63
    Top = 211
    Width = 75
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 5
    OnClick = btn2Click
  end
  object lbledt4: TLabeledEdit
    Left = 11
    Top = 178
    Width = 150
    Height = 21
    EditLabel.Width = 26
    EditLabel.Height = 13
    EditLabel.Caption = #1060#1072#1081#1083
    TabOrder = 4
  end
  object lbledt5: TLabeledEdit
    Left = 11
    Top = 139
    Width = 178
    Height = 21
    EditLabel.Width = 37
    EditLabel.Height = 13
    EditLabel.Caption = #1057#1077#1088#1074#1077#1088
    TabOrder = 6
  end
  object dlgOpen1: TOpenDialog
    Filter = 'Interbase DataBase|*.gdb;*.fdb|Firebird DataBase|*.fdb'
    Left = 16
    Top = 192
  end
end
