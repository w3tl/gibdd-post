unit DBSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Vars, IniFiles;

type
  TForm3 = class(TForm)
    lbledt1: TLabeledEdit;
    lbledt2: TLabeledEdit;
    lbledt3: TLabeledEdit;
    btn1: TButton;
    btn2: TButton;
    lbledt4: TLabeledEdit;
    dlgOpen1: TOpenDialog;
    lbledt5: TLabeledEdit;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.btn2Click(Sender: TObject);
begin
  if Length(lbledt5.Text) = 0 then
    lbledt5.Text := 'localhost';
  if (lbledt5.Text = 'localhost') and (not FileExists(lbledt4.Text)) then
  begin
    ShowMessage('������ ������������ ���� � ����� ���� ������!');
    Exit;
  end;
  dbLogin := lbledt1.Text;
  dbPassword := lbledt2.Text;
  dbPath := lbledt4.Text;
  dbServer := lbledt5.Text;
  dbLCType := lbledt3.Text;
  Ini := TIniFile.Create(IniPath);
  Ini.WriteString('Database', 'login', dbLogin);
  Ini.WriteString('Database', 'password', dbPassword);
  Ini.WriteString('Database', 'path', dbPath);
  Ini.WriteString('Database', 'server', dbServer);
  Ini.WriteString('Database', 'lctype', dbLCType);
  Close;
end;

procedure TForm3.btn1Click(Sender: TObject);
begin
  if dlgOpen1.Execute then
    lbledt4.Text := dlgOpen1.FileName;
end;

end.
