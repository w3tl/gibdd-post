unit ReportSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Vars, IniFiles, DataModule;

type
  TForm4 = class(TForm)
    lbledt1: TLabeledEdit;
    lbledt2: TLabeledEdit;
    lbledt3: TLabeledEdit;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    btn7: TButton;
    dlgOpen1: TOpenDialog;
    lbledt4: TLabeledEdit;
    btn8: TButton;
    dlgOpen2: TOpenDialog;
    lbledt5: TLabeledEdit;
    btn9: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure btn8Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

{$R *.dfm}

procedure TForm4.btn1Click(Sender: TObject);
begin
  if dlgOpen1.Execute then
  begin
    case (Sender as TButton).Tag of
      1: lbledt1.Text := dlgOpen1.FileName;
      2: lbledt2.Text := dlgOpen1.FileName;
      3: lbledt3.Text := dlgOpen1.FileName;
    end;
  end;  
end;

procedure TForm4.btn7Click(Sender: TObject);
begin
  if FileExists(lbledt1.Text) and FileExists(lbledt2.Text) and
     FileExists(lbledt3.Text) then
  begin
    repReg := lbledt1.Text;
    repBlank := lbledt2.Text;
    repBack := lbledt3.Text;
    repExcelReg := lbledt4.Text;
    repExcelLost := lbledt5.Text;
    Ini := TIniFile.Create(IniPath);
    Ini.WriteString('Reports', 'reg', repReg);
    Ini.WriteString('Reports', 'blank', repBlank);
    Ini.WriteString('Reports', 'back', repBack);
    Ini.WriteString('Reports', 'excelReg', repExcelReg);
    Ini.WriteString('Reports', 'excelLost', repExcelLost);
    Ini.Free;
    Close;
  end
  else
  begin
    ShowMessage('������ ������������ ���� � ������!');
    Exit;
  end;  
end;

procedure TForm4.btn4Click(Sender: TObject);
var file_path: string;
begin
  try
    case (Sender as TButton).Tag of
      1: file_path := lbledt1.Text;
      2: file_path := lbledt2.Text;
      3: file_path := lbledt3.Text;
    end;
    if not  FileExists(file_path) then
    begin
      ShowMessage('�������� ��� �����!');
      Exit;
    end;
    DataModule2.frReport1.LoadFromFile(file_path);
    
    DataModule2.frReport1.DesignReport;
  except
    ShowMessage('������ �������� �����!');  
  end;
end;

procedure TForm4.btn2Click(Sender: TObject);
begin
  Form4.btn1Click(Sender);
end;

procedure TForm4.btn5Click(Sender: TObject);
begin
  Form4.btn4Click(Sender);
end;

procedure TForm4.btn3Click(Sender: TObject);
begin
  Form4.btn1Click(Sender);
end;

procedure TForm4.btn6Click(Sender: TObject);
begin
  Form4.btn4Click(Sender);
end;

procedure TForm4.btn8Click(Sender: TObject);
begin
  if dlgOpen2.Execute then
    lbledt4.Text := dlgOpen2.FileName;
end;

procedure TForm4.btn9Click(Sender: TObject);
begin
  if dlgOpen2.Execute then
    lbledt5.Text := dlgOpen2.FileName;
end;

end.
