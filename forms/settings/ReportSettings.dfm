object Form4: TForm4
  Left = 631
  Top = 188
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1092#1086#1088#1084' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 279
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbledt1: TLabeledEdit
    Left = 15
    Top = 27
    Width = 337
    Height = 21
    EditLabel.Width = 108
    EditLabel.Height = 13
    EditLabel.Caption = #1056#1077#1077#1089#1090#1088' '#1055#1086#1095#1090#1099' '#1056#1086#1089#1089#1080#1080
    TabOrder = 2
  end
  object lbledt2: TLabeledEdit
    Left = 15
    Top = 75
    Width = 337
    Height = 21
    EditLabel.Width = 100
    EditLabel.Height = 13
    EditLabel.Caption = #1041#1083#1072#1085#1082' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1081
    TabOrder = 5
  end
  object lbledt3: TLabeledEdit
    Left = 15
    Top = 123
    Width = 337
    Height = 21
    EditLabel.Width = 100
    EditLabel.Height = 13
    EditLabel.Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1089#1090#1086#1088#1086#1085#1072
    TabOrder = 8
  end
  object btn1: TButton
    Tag = 1
    Left = 360
    Top = 24
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 0
    OnClick = btn1Click
  end
  object btn2: TButton
    Tag = 2
    Left = 360
    Top = 72
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 3
    OnClick = btn2Click
  end
  object btn3: TButton
    Tag = 3
    Left = 360
    Top = 120
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 6
    OnClick = btn3Click
  end
  object btn4: TButton
    Tag = 1
    Left = 392
    Top = 24
    Width = 75
    Height = 25
    Caption = #1055#1088#1086#1089#1084#1086#1090#1088
    TabOrder = 1
    OnClick = btn4Click
  end
  object btn5: TButton
    Tag = 2
    Left = 392
    Top = 72
    Width = 75
    Height = 25
    Caption = #1055#1088#1086#1089#1084#1086#1090#1088
    TabOrder = 4
    OnClick = btn5Click
  end
  object btn6: TButton
    Tag = 3
    Left = 392
    Top = 120
    Width = 75
    Height = 25
    Caption = #1055#1088#1086#1089#1084#1086#1090#1088
    TabOrder = 7
    OnClick = btn6Click
  end
  object btn7: TButton
    Left = 204
    Top = 240
    Width = 75
    Height = 25
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    TabOrder = 9
    OnClick = btn7Click
  end
  object lbledt4: TLabeledEdit
    Left = 15
    Top = 171
    Width = 337
    Height = 21
    EditLabel.Width = 84
    EditLabel.Height = 13
    EditLabel.Caption = #1064#1072#1073#1083#1086#1085' '#1088#1077#1077#1089#1090#1088#1072
    TabOrder = 10
  end
  object btn8: TButton
    Tag = 3
    Left = 360
    Top = 168
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 11
    OnClick = btn8Click
  end
  object lbledt5: TLabeledEdit
    Left = 15
    Top = 211
    Width = 337
    Height = 21
    EditLabel.Width = 185
    EditLabel.Height = 13
    EditLabel.Caption = #1064#1072#1073#1083#1086#1085' '#1087#1086#1090#1077#1088#1103#1085#1085#1099#1093' '#1087#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1081
    TabOrder = 12
  end
  object btn9: TButton
    Tag = 3
    Left = 360
    Top = 208
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 13
    OnClick = btn9Click
  end
  object dlgOpen1: TOpenDialog
    Filter = 'FreeReport Form|*.frf'
    Left = 16
    Top = 232
  end
  object dlgOpen2: TOpenDialog
    Filter = #1044#1086#1082#1091#1084#1077#1085#1090#1099' Excel|*.xls;*.xlsx|'#1064#1072#1073#1083#1086#1085#1099' Excel|*.xlst,*.xltx;xltm'
    Left = 56
    Top = 232
  end
end
