unit AutoChangeSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Vars, IniFiles, DataModule, Grids, DBGrids, StdCtrls, Buttons;

type
  TForm8 = class(TForm)
    DBGrid1: TDBGrid;
    btn1: TBitBtn;
    btn2: TBitBtn;
    btn3: TBitBtn;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form8: TForm8;

implementation

{$R *.dfm}

procedure TForm8.btn1Click(Sender: TObject);
var city_from, city_to: string;
begin
  city_from := InputBox('���������� �������', '������� �������� ���������� ������', '');
  if Length(city_from) = 0 then Exit;
  city_to := InputBox('���������� �������', '������� �������� ���������� ������', '�������������-����������');
  if Length(city_to) = 0 then Exit;
  if MessageDlg('�������� ����� �������?' + #10#13 + city_from + ' -> ' + city_to,
                 mtConfirmation, [mbYes, mbNo], 0) = mrNo then Exit;
                  
  try
    with DataModule2.IBWorkChangeQuery do
    begin
      Close;
      SQL.Clear;
      SQL.Add('INSERT INTO CHANGE_CITY (ID, CITY_FROM, CITY_TO) VALUES(:id, :from, :to)');
      ParamByName('id').AsInteger := Random(High(Integer));
      ParamByName('from').AsString := city_from;
      ParamByName('to').AsString := city_to;
      ExecSQL;
      Transaction.Commit;
    end;
    ShowMessage('������� ���������!');
  except
    MessageDlg('�������� ������ ��� ���������� �������', mtError, [mbOK], 0);
  end;
  with DataModule2.IBChangeQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM CHANGE_CITY');
    Open;
  end;
  DBGrid1.SetFocus;
end;

procedure TForm8.btn2Click(Sender: TObject);
var city_from, city_to: string; id: Integer;
begin
  with DataModule2.IBChangeQuery do
  begin
    id := VarToInt(FieldValues['ID']);
    city_from := VarToStr(FieldValues['CITY_FROM']);
    city_to := VarToStr(FieldValues['CITY_TO']);
  end;
  city_from := InputBox('��������� �������', '������� ����� �������� ���������� ������', city_from);
  if Length(city_from) = 0 then Exit;
  city_to := InputBox('��������� �������', '������� ����� �������� ���������� ������', city_to);
  if Length(city_to) = 0 then Exit;
  if MessageDlg('�������� ������� ������� ��:' + #10#13 + city_from + ' -> ' + city_to + '?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrNo then Exit;

  with DataModule2.IBWorkChangeQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add('UPDATE CHANGE_CITY SET CITY_FROM=:from, CITY_TO=:to WHERE ID=:id');
    ParamByName('from').AsString := city_from;
    ParamByName('to').AsString := city_to;
    ParamByName('id').AsInteger := id;
    ExecSQL;
    Transaction.Commit;
  end;
  DataModule2.IBChangeQuery.Close;
  DataModule2.IBChangeQuery.Open;
end;

procedure TForm8.btn3Click(Sender: TObject);
var city_from, city_to: string; id: Integer;
begin
  with DataModule2.IBChangeQuery do
  begin
    id := VarToInt(FieldValues['ID']);
    city_from := VarToStr(FieldValues['CITY_FROM']);
    city_to := VarToStr(FieldValues['CITY_TO']);
  end;
  if MessageDlg('������ �������?' + #10#13 + city_from + ' -> ' + city_to,
                 mtConfirmation, [mbYes, mbNo], 0) = mrNo then Exit;

  with DataModule2.IBWorkChangeQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add('DELETE FROM CHANGE_CITY WHERE ID=:id');
    ParamByName('id').AsInteger := id;
    ExecSQL;
    Transaction.Commit;
  end;
  DBGrid1.SetFocus;
end;

procedure TForm8.FormCreate(Sender: TObject);
begin
  DBGrid1.DataSource := DataModule2.ds3;
end;

end.
