unit ImportSettings;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, Vars, IniFiles;

type
  TForm5 = class(TForm)
    lbl1: TLabel;
    btn1: TButton;
    edt1: TEdit;
    strngrd1: TStringGrid;
    Label1: TLabel;
    Edit1: TEdit;
    lbl2: TLabel;
    edt2: TEdit;
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form5: TForm5;

implementation

{$R *.dfm}

procedure TForm5.btn1Click(Sender: TObject);
var i: Integer;
begin
  try
    Ini := TIniFile.Create(IniPath);
    xlStartRow := StrToInt(edt1.Text);
    regNumber := StrToInt(Edit1.Text);
    regDate := StrToInt(edt2.Text);
    Ini.WriteInteger('Import', 'StartRow', xlStartRow);
    Ini.WriteInteger('Import', 'regNumber', regNumber);
    Ini.WriteInteger('Import', 'regDate', regDate);

    for i := 0 to tableFinesFields.Count - 1 do
    begin
      tableFinesPos[i] := StrToInt(strngrd1.Cells[1, i]);
      Ini.WriteInteger('Table', tableFinesFields[i], tableFinesPos[i]);
    end;
    Ini.Free;
    Close;
  except
    ShowMessage('������� �������� ��������!');
    Exit;
  end;
end;

end.
