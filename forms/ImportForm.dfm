object Form2: TForm2
  Left = 233
  Top = 455
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081' '#1089#1087#1080#1089#1086#1082
  ClientHeight = 455
  ClientWidth = 665
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl2: TLabel
    Left = 18
    Top = 152
    Width = 3
    Height = 13
  end
  object lbledt1: TLabeledEdit
    Left = 16
    Top = 19
    Width = 513
    Height = 21
    EditLabel.Width = 26
    EditLabel.Height = 13
    EditLabel.Caption = #1060#1072#1081#1083
    TabOrder = 1
  end
  object btn2: TButton
    Left = 158
    Top = 177
    Width = 105
    Height = 25
    Caption = #1048#1084#1087#1086#1088#1090
    TabOrder = 8
    OnClick = btn2Click
  end
  object pb1: TProgressBar
    Left = 0
    Top = 212
    Width = 665
    Height = 17
    Align = alBottom
    Step = 1
    TabOrder = 11
  end
  object StringGrid1: TStringGrid
    Left = 0
    Top = 229
    Width = 665
    Height = 207
    Align = alBottom
    ColCount = 2
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
    TabOrder = 12
    ColWidths = (
      64
      64)
  end
  object stat1: TStatusBar
    Left = 0
    Top = 436
    Width = 665
    Height = 19
    Panels = <
      item
        Text = #1047#1072#1087#1080#1089#1077#1081':'
        Width = 60
      end
      item
        Width = 50
      end
      item
        Text = #1053#1077' '#1087#1086#1083#1085#1099#1093':'
        Width = 80
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object btn1: TBitBtn
    Left = 544
    Top = 17
    Width = 105
    Height = 25
    Caption = #1042#1099#1073#1088#1072#1090#1100
    TabOrder = 0
    OnClick = btn1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFF6F6F6F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2
      F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F2F8F8F8FFFFFFE5E5E5999999
      9999999999999999999999999999999999999999999999999999999999999999
      99999999999999E5E5E5FFFFFFAAA299E2D3C2E2D3C2E2D3C2E2D3C2E2D3C2E2
      D3C2E2D3C2E2D3C2E2D3C2E2D3C2E2D3C2E2D3C2AAA29AFFFFFFFFFFFFAFA193
      D8C4AFD8C4AFD8C4AFD8C4AFD8C4AFD8C4AFD8C4AFD8C4AFD8C4AFD8C4AFD8C4
      AFD8C4AFAFA193FFFFFFFFFFFFAFA193D1BAA1D1BAA1D1BAA1D1BAA1D1BAA1D1
      BAA1D1BAA1D1BAA1D1BAA1D1BAA1D1BAA1D1BAA1AFA193FFFFFFFFFFFFAFA193
      CBB194CBB194CBB194CBB194CBB194CBB194CBB194CBB194CBB194CBB194CBB1
      94CBB194AFA193FFFFFFFFFFFFAFA193D0B69AD0B69AD0B69AD0B69AD0B69AD0
      B69AD0B69AD0B69AD0B69AD0B69AD0B69AD0B69AAFA193FFFFFFFFFFFFAFA193
      E7DACBE7DACBE7DACBE7DACBE7DACBE7DACBE7DACBE7DACBE7DACBE7DACBE7DA
      CBB9AEA2AFA193FFFFFFFFFFFFAFA193A1907EA1907EA1907EA1907EA1907EA1
      907EA1907EA1907EA1907EA1907EA1907EA1907EAFA193FFFFFFFFFFFFAFA193
      BCAB98BCAB98BCAB98BCAB98BCAB98BCAB98BCAB98BCAB98BCAB98BCAB98BCAB
      98BCAB98AFA193FFFFFFFFFFFFAFA193D1BEA9D1BEA9D1BEA9D1BEA9D1BEA9D1
      BEA9D1BEA9D1BEA9D1BEA9D1BEA9D1BEA9D1BEA9AFA193FFFFFFFFFFFFAFA193
      D4C1ACD4C1ACD4C1ACD4C1ACD4C1ACD8C7B4D8C7B4D8C7B4D8C7B4D8C7B4D8C7
      B4DDCDBCAFA193FFFFFFFFFFFFC3B4A3DDCDBCDDCDBCDDCDBCDDCDBCDDCDBCC3
      B4A3C3B4A3C3B4A3C3B4A3C3B4A3C3B4A3C3B4A3CFC3B5FFFFFFFFFFFFFFFFFF
      C3B4A3C3B4A3C3B4A3C3B4A3C3B4A3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
  end
  object grp1: TGroupBox
    Left = 16
    Top = 48
    Width = 633
    Height = 57
    Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
    TabOrder = 2
    object lbl3: TLabel
      Left = 40
      Top = 24
      Width = 97
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1089#1090#1088#1086#1082#1072':'
    end
    object lbl4: TLabel
      Left = 204
      Top = 24
      Width = 111
      Height = 13
      Caption = #1057#1083#1077#1076'. '#1085#1086#1084#1077#1088' '#1088#1077#1077#1089#1090#1088#1072':'
    end
    object lbl5: TLabel
      Left = 381
      Top = 24
      Width = 158
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103' ('#1089#1090#1086#1083#1073#1077#1094')'
    end
    object edtStartRow: TEdit
      Left = 150
      Top = 20
      Width = 41
      Height = 21
      TabOrder = 0
    end
    object edtNextNumber: TEdit
      Left = 327
      Top = 20
      Width = 41
      Height = 21
      Enabled = False
      TabOrder = 1
    end
    object edtResDate: TEdit
      Left = 552
      Top = 20
      Width = 41
      Height = 21
      TabOrder = 2
    end
  end
  object btn3: TBitBtn
    Left = 282
    Top = 177
    Width = 105
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 9
    OnClick = btn3Click
    Kind = bkOK
  end
  object btn4: TBitBtn
    Left = 405
    Top = 177
    Width = 105
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074#1089#1077
    TabOrder = 10
    OnClick = btn4Click
    Kind = bkAll
  end
  object lbledt2: TLabeledEdit
    Left = 115
    Top = 148
    Width = 168
    Height = 21
    EditLabel.Width = 92
    EditLabel.Height = 13
    EditLabel.Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1088#1077#1077#1089#1090#1088#1072
    MaxLength = 25
    TabOrder = 4
  end
  object dtp1: TDateTimePicker
    Left = 305
    Top = 148
    Width = 145
    Height = 21
    Date = 41751.794810023150000000
    Time = 41751.794810023150000000
    TabOrder = 5
    OnChange = dtp1Change
  end
  object txt1: TStaticText
    Left = 305
    Top = 127
    Width = 30
    Height = 17
    Caption = #1044#1072#1090#1072
    TabOrder = 3
  end
  object lbledt4: TLabeledEdit
    Left = 471
    Top = 148
    Width = 80
    Height = 21
    EditLabel.Width = 75
    EditLabel.Height = 13
    EditLabel.Caption = #1053#1086#1084#1077#1088' '#1088#1077#1077#1089#1090#1088#1072
    TabOrder = 6
  end
  object btn5: TButton
    Left = 584
    Top = 152
    Width = 75
    Height = 25
    Caption = 'btn5'
    TabOrder = 7
    Visible = False
    OnClick = btn5Click
  end
  object dlgOpen1: TOpenDialog
    Filter = 
      'Excel 2003-2012|*.xlsx;*.xls|Excel 2007-2012|*.xlsx|Excel 2003|*' +
      '.xls'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 72
    Top = 128
  end
end
