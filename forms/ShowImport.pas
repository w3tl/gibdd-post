unit ShowImport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, Vars;

type
  TForm13 = class(TForm)
    strngrd1: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form13: TForm13;

implementation

{$R *.dfm}

procedure TForm13.FormCreate(Sender: TObject);
var colWidth, i: Integer;
begin
  strngrd1.ColCount := tableFinesFields.Count;
  colWidth := Round(strngrd1.ClientWidth / tableFinesFields.Count);
  for i := 0 to tableFinesFields.Count - 1 do
  begin
    strngrd1.ColWidths[i] := colWidth;
  end;
end;

procedure TForm13.FormResize(Sender: TObject);
var colWidth, i: Integer;
begin
  strngrd1.ColCount := tableFinesFields.Count;
  colWidth := Round(strngrd1.ClientWidth / tableFinesFields.Count);
  for i := 0 to tableFinesFields.Count - 1 do
  begin
    strngrd1.ColWidths[i] := colWidth;
  end;
end;

end.
