unit ChangeForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TForm12 = class(TForm)
    lbl1: TLabel;
    edt1: TEdit;
    lbl2: TLabel;
    lbl3: TLabel;
    dtp1: TDateTimePicker;
    dtp2: TDateTimePicker;
    btn1: TButton;
    lbl4: TLabel;
    edt2: TEdit;
    lbl5: TLabel;
    edt3: TEdit;
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form12: TForm12;

implementation

{$R *.dfm}

procedure TForm12.btn1Click(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TForm12.FormCreate(Sender: TObject);
begin
  dtp1.Date := Now;
  dtp2.Date := Now;
end;

end.
