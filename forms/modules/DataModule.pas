unit DataModule;

interface

uses
  SysUtils, Classes, FR_Class, DB, IBCustomDataSet, IBQuery, IBDatabase,
  FR_DSet, FR_DBSet;

type
  TDataModule2 = class(TDataModule)
    IBDatabase1: TIBDatabase;
    IBTransaction1: TIBTransaction;
    IBTransaction2: TIBTransaction;
    IBQuery1: TIBQuery;
    IBQuery2: TIBQuery;
    IBWorkQuery2: TIBQuery;
    IBWorkQuery1: TIBQuery;
    frReport1: TfrReport;
    ds1: TDataSource;
    ds2: TDataSource;
    frDBDataSet1: TfrDBDataSet;
    IBQuery3: TIBQuery;
    IBTransaction3: TIBTransaction;
    IBChangeQuery: TIBQuery;
    IBWorkChangeQuery: TIBQuery;
    ds3: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModule2: TDataModule2;

implementation

{$R *.dfm}

end.
