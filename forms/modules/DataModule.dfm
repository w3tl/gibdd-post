object DataModule2: TDataModule2
  OldCreateOrder = False
  Left = 192
  Top = 124
  Height = 287
  Width = 378
  object IBDatabase1: TIBDatabase
    Left = 104
    Top = 8
  end
  object IBTransaction1: TIBTransaction
    DefaultDatabase = IBDatabase1
    Left = 86
    Top = 56
  end
  object IBTransaction2: TIBTransaction
    DefaultDatabase = IBDatabase1
    Left = 126
    Top = 56
  end
  object IBQuery1: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Left = 74
    Top = 104
  end
  object IBQuery2: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction2
    Left = 136
    Top = 104
  end
  object IBWorkQuery2: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction2
    Left = 168
    Top = 104
  end
  object IBWorkQuery1: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Left = 42
    Top = 104
  end
  object frReport1: TfrReport
    Dataset = frDBDataSet1
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    Left = 32
    Top = 200
    ReportForm = {17000000}
  end
  object ds1: TDataSource
    DataSet = IBQuery1
    Left = 72
    Top = 152
  end
  object ds2: TDataSource
    DataSet = IBQuery2
    Left = 136
    Top = 152
  end
  object frDBDataSet1: TfrDBDataSet
    DataSet = IBQuery2
    OpenDataSource = False
    Left = 80
    Top = 200
  end
  object IBQuery3: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Left = 8
    Top = 104
  end
  object IBTransaction3: TIBTransaction
    DefaultDatabase = IBDatabase1
    Left = 240
    Top = 56
  end
  object IBChangeQuery: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction3
    Left = 232
    Top = 104
  end
  object IBWorkChangeQuery: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction3
    Left = 264
    Top = 104
  end
  object ds3: TDataSource
    DataSet = IBChangeQuery
    Left = 248
    Top = 152
  end
end
