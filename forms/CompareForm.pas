unit CompareForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, DataModule, ComObj, Vars, jpeg;

type
  TForm9 = class(TForm)
    lbledt1: TLabeledEdit;
    dlgOpen1: TOpenDialog;
    dlgSave1: TSaveDialog;
    btn1: TButton;
    btn3: TButton;
    pb1: TProgressBar;
    lbl1: TLabel;
    edt1: TEdit;
    lbl2: TLabel;
    edt2: TEdit;
    rb1: TRadioButton;
    rb2: TRadioButton;
    rg1: TRadioGroup;
    lbl3: TLabel;
    img1: TImage;
    lbl4: TLabel;
    img2: TImage;
    procedure btn1Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  procedure CompareDefective;
  procedure CompareLost;
  function getCompareField: string;
  
var
  Form9: TForm9;
  StartRow, CompareCol: Integer;

implementation

{$R *.dfm}

function getCompareField: string;
begin
  if Form9.rg1.ItemIndex = 0 then
    Result := 'RESOLUTION';
  if Form9.rg1.ItemIndex = 1 then
    Result := 'BARCODE';
end;  

procedure TForm9.btn1Click(Sender: TObject);
begin
  if dlgOpen1.Execute then
    lbledt1.Text := dlgOpen1.FileName;
end;

procedure CompareDefective;
var WorkSheet, WorkBook, Range, URange: Variant; FData: OleVariant; Rows, i: Integer;
begin
  try
    try
      try
        Excel := GetActiveOleObject('Excel.Application');
      except
        Excel := CreateOleObject('Excel.Application');
      end;
      WorkBook := Excel.Workbooks.Open(Form9.lbledt1.Text, 0, True);
      Excel.DisplayAlerts := False;
    except
        raise Exception.Create('������ �������� �����!' + #10#13 +
                    '���������, ��� ���� ���������� � ��� ���� Excel �������.');
    end;
    WorkSheet := WorkBook.ActiveSheet;
    URange := WorkSheet.UsedRange;
    if URange.Rows.Count < StartRow then // ������ ����� ��������� ������� ���
      raise Exception.Create('� ������� ����������� ������!' + #10#13 +
                             '�������, ������� ������������ ��������� ������.');
    URange := Excel.Intersect(URange, URange.Offset[StartRow - 1, 0]); // ������ �� ������� ��������� �������
    Rows := URange.Rows.Count;
    try
      FData := URange.Value;
    except
      raise Exception.Create('������ ������ �����! ' + #10#13 +
                             '�������� �������������� ����� Excel ����� ��.');
    end;
    Form9.pb1.Max := Rows;
    Form9.pb1.Position := 0;
    for i := Rows downto 1 do begin
      try
        with DataModule2.IBQuery2 do
        begin
          Close;
          SQL.Clear;
          SQL.Add('SELECT REG_ID FROM FINES WHERE ' + getCompareField + '=:data');
          ParamByName('data').AsString := FData[i, CompareCol];
          Open;
          if RecordCount = 0 then
            URange.Rows[i].Delete;
        end;
      except
        if MessageDlg('������ ������ ��������, ��������, ������ ������� �������.' +
          '����������?',
          mtError, [mbOk, mbNo], 0) = mrNo then
        raise Exception.Create('�������� ��������!');
      end;
      Form9.pb1.Position := Form9.pb1.Position + 1;
    end;
    Form9.dlgSave1.FileName := '�����������' + DateToStr(Now);
    if Form9.dlgSave1.Execute then
      WorkBook.SaveAs(Form9.dlgSave1.FileName, xlExcel8);
    MessageDlg('���� � ���������� ��������������� ������!', mtInformation, [mbOK], 0);
  except on E: Exception do
    begin
      MessageDlg(E.Message, mtError, [mbOk], 0);
      CloseExcel;
      Form9.pb1.Max := 0;
      Form9.pb1.Position := 0;
      Exit;
    end;
  end;
  CloseExcel;
end;

procedure CompareLost;
var WorkSheet, WorkBook, Range, URange: Variant; FData, PutData: OleVariant; Rows, i, j: Integer;
  TmpData: Variant; foundSize: Integer;
begin
  try
    try
      try
        Excel := GetActiveOleObject('Excel.Application');
      except
        Excel := CreateOleObject('Excel.Application');
      end;
      WorkBook := Excel.Workbooks.Open(Form9.lbledt1.Text, 0, True);
      Excel.DisplayAlerts := False;
    except
        raise Exception.Create('������ �������� �����!' + #10#13 +
                    '���������, ��� ���� ���������� � ��� ���� Excel �������.');
    end;
    WorkSheet := WorkBook.ActiveSheet;
    URange := WorkSheet.UsedRange;
    if URange.Rows.Count < StartRow then // ������ ����� ��������� ������� ���
      raise Exception.Create('� ������� ����������� ������!' + #10#13 +
                             '�������, ������� ������������ ��������� ������.');
    URange := Excel.Intersect(URange, URange.Offset[StartRow - 1, 0]); // ������ �� ������� ��������� �������
    Rows := URange.Rows.Count;
    try
      FData := URange.Value; // �������� ������
    except
      raise Exception.Create('������ ������ �����! ' + #10#13 +
                             '�������� �������������� ����� Excel ����� ��.');
    end;
    WorkBook.Close;
    
    try
      try
        Excel := GetActiveOleObject('Excel.Application');
      except
        Excel := CreateOleObject('Excel.Application');
      end;
      Excel.Visible := False;
      WorkBook := Excel.Workbooks.Open(repExcelLost, 0, True);
      Excel.DisplayAlerts := False;
    except
        raise Exception.Create('������ �������� �����!' + #10#13 +
                    '���������, ��� ���� ���������� � ��� ���� Excel �������.');
    end;
    WorkSheet := WorkBook.ActiveSheet;
    Form9.pb1.Max := Rows;
    Form9.pb1.Position := 0;
    TmpData := VarArrayCreate([1, 0], varVariant);
    for i := 1 to Rows do begin
      try
        with DataModule2.IBQuery2 do
        begin
          Close;                      
          SQL.Clear;
          SQL.Add('SELECT FIRST 1 A.REG_ID,A.ADRESAT,A.CITY,A.ADDRESS,A.RESOLUTION,A.BARCODE,B.POST_DATE FROM FINES AS A ' +
                  'INNER JOIN REGISTRIES AS B ON A.REG_ID=B.ID WHERE A.' + getCompareField + '=:data');
          ParamByName('data').AsString := FData[i, CompareCol];
          Open;
          FetchAll;
          if RecordCount > 0 then
          begin
            VarArrayRedim(TmpData, VarArrayHighBound(TmpData, 1) + 1);
            TmpData[VarArrayHighBound(TmpData, 1)] :=
                VarArrayOf([VarArrayHighBound(TmpData, 1),
                            FieldValues['ADRESAT'],
                            FieldValues['CITY'],
                            FieldValues['ADDRESS'],
                            FieldValues['RESOLUTION'],
                            FieldValues['BARCODE'],
                            FieldValues['POST_DATE']]);
          end;
        end;
      except on E: Exception do
        begin
          if MessageDlg('������ ������ ��������, ��������, ������ ������� �������. ' +
            '����������?' + #10#13 + E.Message,
            mtError, [mbOk, mbNo], 0) = mrNo then
          raise Exception.Create('�������� ��������!');
        end;
      end;
      Form9.pb1.Position := Form9.pb1.Position + 1;
    end;
    if VarArrayHighBound(TmpData, 1) > 1 then
    begin
      foundSize := VarArrayHighBound(TmpData, 1);
      Range := WorkSheet.Range['A5:G' + IntToStr(foundSize + 3)];
      Range.EntireRow.Insert(xlShiftDown, xlFormatFromLeftOrAbove);
      Range := WorkSheet.Range['A5:G' + IntToStr(foundSize + 4)];
      PutData := VarArrayCreate([1, foundSize, 1, 7], varVariant);
      for i := 1 to foundSize do
        for j := 1 to 7 do
        begin
          PutData[i, j] := TmpData[i][j - 1];
        end;
      Range.Value := PutData;
      VarClear(PutData);
    end;
    VarClear(FData);

    Form9.dlgSave1.FileName := '���������� ' + DateToStr(Now);
    if Form9.dlgSave1.Execute then begin
      WorkBook.SaveAs(Form9.dlgSave1.FileName, xlExcel8);
      WorkBook.Activate;
    end else begin
      WorkBook.Close;
      MessageDlg('���� �� ��� ��������!', mtInformation, [mbOK], 0);
    end;
  except on E: Exception do
    begin
      MessageDlg(E.Message, mtError, [mbOk], 0);
      CloseExcel;
      Form9.pb1.Max := 0;
      Form9.pb1.Position := 0;
      Exit;
    end;
  end;
  Excel.Visible := True;
  Form9.Close;
end;

procedure TForm9.btn3Click(Sender: TObject);
begin
  try
    StartRow := StrToInt(edt1.Text);
    CompareCol := StrToInt(edt2.Text);
  except
    MessageDlg('��������� ��������� ��������� ������ � �������!',
               mtError, [mbOK], 0);
    Exit;
  end;
  if not FileExists(lbledt1.Text) then begin
    MessageDlg('��������� ���� �� ������!', mtError, [mbOK], 0);
    Exit;
  end;
  if rb1.Checked then CompareDefective;
  if rb2.Checked then CompareLost;
end;

end.
