unit ReportDialog;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Vars, StdCtrls, ComCtrls;

type
  TForm11 = class(TForm)
    chk1: TCheckBox;
    lbl1: TLabel;
    dtp1: TDateTimePicker;
    btn1: TButton;
    lbl2: TLabel;
    chk2: TCheckBox;
    lbl3: TLabel;
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form11: TForm11;

implementation

{$R *.dfm}

procedure TForm11.btn1Click(Sender: TObject);
begin
  reportSort := chk1.Checked;
  reportDate := dtp1.Date;
  ModalResult := mrOk;
end;

procedure TForm11.FormCreate(Sender: TObject);
begin
  dtp1.Date := Now;
end;

end.
