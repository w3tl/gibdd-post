object Form11: TForm11
  Left = 256
  Top = 143
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1057#1086#1079#1076#1072#1085#1080#1077' '#1086#1090#1095#1077#1090#1072
  ClientHeight = 161
  ClientWidth = 236
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 16
    Top = 78
    Width = 76
    Height = 13
    Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
  end
  object lbl2: TLabel
    Left = 16
    Top = 96
    Width = 108
    Height = 13
    Caption = '('#1086#1090#1087#1088#1072#1074#1082#1080' '#1085#1072' '#1087#1086#1095#1090#1091'):'
  end
  object lbl3: TLabel
    Left = 32
    Top = 57
    Width = 167
    Height = 13
    Caption = #1085#1072' '#1087#1086#1095#1090#1091' '#1076#1083#1103' '#1091#1078#1077' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1085#1099#1093
  end
  object chk1: TCheckBox
    Left = 16
    Top = 16
    Width = 169
    Height = 17
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086' '#1072#1083#1092#1072#1074#1080#1090#1091
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object dtp1: TDateTimePicker
    Left = 128
    Top = 84
    Width = 97
    Height = 21
    Date = 41751.868435393520000000
    Time = 41751.868435393520000000
    TabOrder = 1
  end
  object btn1: TButton
    Left = 79
    Top = 128
    Width = 75
    Height = 25
    Caption = #1044#1072#1083#1077#1077
    TabOrder = 2
    OnClick = btn1Click
  end
  object chk2: TCheckBox
    Left = 16
    Top = 40
    Width = 217
    Height = 17
    Caption = #1053#1077' '#1091#1089#1090#1072#1085#1072#1074#1083#1080#1074#1072#1090#1100' '#1076#1072#1090#1091' '#1086#1090#1087#1088#1072#1074#1082#1080' '
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
end
