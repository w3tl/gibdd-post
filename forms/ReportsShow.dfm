object Form7: TForm7
  Left = 210
  Top = 145
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = #1055#1077#1095#1072#1090#1100' '#1080' '#1087#1088#1086#1089#1084#1086#1090#1088' '#1086#1090#1095#1077#1090#1086#1074
  ClientHeight = 179
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object rg1: TRadioGroup
    Tag = 1
    Left = 9
    Top = 8
    Width = 336
    Height = 121
    Caption = #1060#1086#1088#1084#1099
    TabOrder = 0
  end
  object btn1: TButton
    Left = 139
    Top = 143
    Width = 75
    Height = 25
    Caption = #1055#1088#1086#1089#1084#1086#1090#1088
    TabOrder = 9
    OnClick = btn1Click
  end
  object rb1: TRadioButton
    Left = 24
    Top = 30
    Width = 65
    Height = 17
    Caption = #1057#1087#1080#1089#1086#1082
    Checked = True
    TabOrder = 3
    TabStop = True
    OnClick = rb1Click
  end
  object rb2: TRadioButton
    Left = 24
    Top = 96
    Width = 153
    Height = 17
    Caption = #1041#1083#1072#1085#1082#1080' '#1091#1074#1077#1076#1086#1084#1083#1077#1085#1080#1081
    TabOrder = 8
  end
  object rb3: TRadioButton
    Left = 24
    Top = 64
    Width = 129
    Height = 17
    Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1089#1090#1086#1088#1086#1085#1072
    TabOrder = 5
  end
  object cbb1: TComboBox
    Left = 96
    Top = 29
    Width = 137
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 1
    Text = #1043#1086#1088#1086#1076
    OnChange = cbb1Change
    Items.Strings = (
      #1043#1086#1088#1086#1076
      #1048#1085#1086#1075#1086#1088#1086#1076#1085#1080#1077
      #1054#1073#1097#1080#1081)
  end
  object cbb2: TComboBox
    Left = 224
    Top = 86
    Width = 97
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 7
    Text = #1055#1086' '#1072#1083#1092#1072#1074#1080#1090#1091
    Items.Strings = (
      #1055#1086' '#1072#1083#1092#1072#1074#1080#1090#1091
      #1055#1086' '#1064#1055#1048
      #1055#1086' '#1087#1086#1088#1103#1076#1082#1091)
  end
  object txt1: TStaticText
    Left = 240
    Top = 31
    Width = 60
    Height = 17
    Caption = #1057#1087#1080#1089#1086#1082' '#8470':'
    TabOrder = 4
  end
  object edt1: TEdit
    Left = 299
    Top = 29
    Width = 33
    Height = 21
    TabOrder = 2
  end
  object txt2: TStaticText
    Left = 224
    Top = 64
    Width = 69
    Height = 17
    Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072':'
    TabOrder = 6
  end
end
