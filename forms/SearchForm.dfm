object Form10: TForm10
  Left = 290
  Top = 242
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1055#1086#1080#1089#1082
  ClientHeight = 388
  ClientWidth = 1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl1: TLabel
    Left = 20
    Top = 36
    Width = 7
    Height = 13
    Caption = #1057
  end
  object lbl2: TLabel
    Left = 117
    Top = 36
    Width = 12
    Height = 13
    Caption = #1087#1086
  end
  object rb1: TRadioButton
    Left = 16
    Top = 8
    Width = 209
    Height = 18
    Caption = #1055#1086' '#1076#1072#1090#1077' '#1074#1099#1085#1077#1089#1077#1085#1080#1103' '#1087#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103
    Checked = True
    TabOrder = 0
    TabStop = True
  end
  object rb2: TRadioButton
    Left = 232
    Top = 8
    Width = 161
    Height = 18
    Caption = #1055#1086' '#1085#1086#1084#1077#1088#1091' '#1087#1086#1089#1090#1072#1085#1086#1074#1083#1077#1085#1080#1103
    TabOrder = 1
  end
  object edt2: TEdit
    Left = 248
    Top = 32
    Width = 145
    Height = 21
    TabOrder = 5
    OnEnter = edt2Enter
  end
  object dtp1: TDateTimePicker
    Left = 31
    Top = 32
    Width = 82
    Height = 22
    Date = 41751.858377581020000000
    Time = 41751.858377581020000000
    TabOrder = 4
    OnClick = dtp1Click
  end
  object dtp2: TDateTimePicker
    Left = 133
    Top = 32
    Width = 83
    Height = 22
    Date = 41751.858982291670000000
    Time = 41751.858982291670000000
    TabOrder = 8
    OnClick = dtp2Click
  end
  object dbgrd1: TDBGrid
    Left = 0
    Top = 72
    Width = 1080
    Height = 297
    Align = alBottom
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = pm1
    TabOrder = 11
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnMouseMove = dbgrd1MouseMove
  end
  object btn1: TButton
    Left = 844
    Top = 24
    Width = 91
    Height = 25
    Caption = #1055#1086#1080#1089#1082
    TabOrder = 3
    OnClick = btn1Click
  end
  object stat1: TStatusBar
    Left = 0
    Top = 369
    Width = 1080
    Height = 19
    Panels = <
      item
        Text = #1053#1072#1081#1076#1077#1085#1086' '#1079#1072#1087#1080#1089#1077#1081':'
        Width = 105
      end
      item
        Width = 50
      end>
  end
  object edt1: TEdit
    Left = 640
    Top = 32
    Width = 137
    Height = 21
    TabOrder = 10
    OnEnter = edt1Enter
  end
  object rb3: TRadioButton
    Left = 640
    Top = 8
    Width = 137
    Height = 18
    Caption = #1055#1086' '#1064#1055#1048
    TabOrder = 7
  end
  object rb4: TRadioButton
    Left = 416
    Top = 8
    Width = 201
    Height = 18
    Caption = #1055#1086' '#1080#1084#1077#1085#1080
    TabOrder = 2
  end
  object edt3: TEdit
    Left = 424
    Top = 32
    Width = 193
    Height = 21
    TabOrder = 6
    OnEnter = edt3Enter
  end
  object btn2: TButton
    Left = 940
    Top = 24
    Width = 91
    Height = 25
    Caption = #1057#1087#1080#1089#1086#1082
    Enabled = False
    TabOrder = 9
    OnClick = btn2Click
  end
  object pm1: TPopupMenu
    Left = 1040
    Top = 8
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      OnClick = N1Click
    end
  end
end
