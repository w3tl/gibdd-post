unit SearchForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids, ComCtrls, DataModule, Vars, ReportsShow,
  Menus;

type
  TForm10 = class(TForm)
    rb1: TRadioButton;
    rb2: TRadioButton;
    edt2: TEdit;
    dtp1: TDateTimePicker;
    lbl1: TLabel;
    lbl2: TLabel;
    dtp2: TDateTimePicker;
    dbgrd1: TDBGrid;
    btn1: TButton;
    stat1: TStatusBar;
    edt1: TEdit;
    rb3: TRadioButton;
    rb4: TRadioButton;
    edt3: TEdit;
    btn2: TButton;
    pm1: TPopupMenu;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure dtp1Click(Sender: TObject);
    procedure dtp2Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure edt3Enter(Sender: TObject);
    procedure edt2Enter(Sender: TObject);
    procedure edt1Enter(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure dbgrd1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
    procedure updateDB;
  public
    result: Integer;
  end;

var
  Form10: TForm10;
  Mouse: TPoint;

implementation

{$R *.dfm}

procedure TForm10.FormCreate(Sender: TObject);
begin
  dbgrd1.DataSource := DataModule2.ds2;
  dtp1.Date := Now;
  dtp2.Date := Now;
end;

procedure TForm10.updateDB;
var colWidth, i: Integer;
begin
  colWidth := Round(dbgrd1.ClientWidth / 6) - 1;
  for i := 0 to dbgrd1.Columns.Count - 1 do
  begin
    case i of
      6, 7: dbgrd1.Columns.Items[i].Visible := False;
      else
        dbgrd1.Columns.Items[i].Width := colWidth;
    end;
  end;
  dbgrd1.Columns[0].Title.Caption := '����� �������';
  dbgrd1.Columns[1].Title.Caption := '��������';
  dbgrd1.Columns[2].Title.Caption := '�������������';
  dbgrd1.Columns[3].Title.Caption := '�������';
  dbgrd1.Columns[4].Title.Caption := '���� �������� �� �����';
  dbgrd1.Columns[5].Title.Caption := '���� ��������';
end;  

procedure TForm10.btn1Click(Sender: TObject);
begin
  if rb1.Checked then
  begin
    try
      with DataModule2.IBQuery2 do
      begin
        Close;
        SQL.Text :=
          'SELECT B.REG_NUMBER, A.BARCODE, A.RESOLUTION, A.ADRESAT, B.POST_DATE, B.CREATE_DATE, ' +
          'A.REG_ID, B.ID ' +
          'FROM FINES AS A INNER JOIN REGISTRIES AS B ON A.REG_ID=B.ID ' +
          'WHERE A.REG_ID IN (SELECT ID FROM REGISTRIES ' +
          'WHERE CREATE_DATE>=:start AND CREATE_DATE<=:end)';
        ParamByName('start').AsDate := dtp1.Date;
        ParamByName('end').AsDate := dtp2.Date;
        Open;
        FetchAll;
      end;
      btn2.Enabled := False;
    except
      ShowMessage('������ � ������ �� ����!');
      Exit;
    end;
  end
  else
  begin
    with DataModule2.IBQuery2 do
    begin
      Close;
      SQL.Text := 'SELECT REGISTRIES.REG_NUMBER, FINES.BARCODE, FINES.RESOLUTION, FINES.ADRESAT, REGISTRIES.POST_DATE, REGISTRIES.CREATE_DATE, ' +
                  'FINES.REG_ID, REGISTRIES.ID ' +
                  'FROM FINES INNER JOIN REGISTRIES ON ' +
                  'FINES.REG_ID=REGISTRIES.ID WHERE FINES.';
    end;
    if rb2.Checked then // �� �������������
    begin
      try
        DataModule2.IBQuery2.SQL.Add('RESOLUTION=:res');
        DataModule2.IBQuery2.ParamByName('res').AsString := edt2.Text;
      except
        ShowMessage('������ � ������ �� ������ �������������!');
        edt2.SetFocus;
        Exit;
      end;
    end;
    if rb3.Checked then // �� ���
    begin
      try
        DataModule2.IBQuery2.SQL.Add('BARCODE=:bar');
        DataModule2.IBQuery2.ParamByName('bar').AsString := edt1.Text;
      except
        ShowMessage('������ � ������ �� ���!');
        edt1.SetFocus;
        Exit;
      end;
    end;
    if rb4.Checked then // �� �����
    begin
      try
        DataModule2.IBQuery2.SQL.Add('ADRESAT LIKE ''%' + AnsiUpperCase(edt3.Text) + '%''');
      except
        ShowMessage('������ ������ �� �����!');
        edt3.SetFocus;
        Exit;
      end;
    end;
    try
      DataModule2.IBQuery2.Open;
      DataModule2.IBQuery2.FetchAll;
      DataModule2.IBQuery2.First;
      //DataModule2.IBQuery1.Close;
      //DataModule2.IBQuery1.SQL.Text := 'SELECT * FROM REGISTRIES WHERE ID=' +
      //    VarToStr(DataModule2.IBQuery2.FieldValues['REG_ID']);
      //DataModule2.IBQuery1.Open;
      btn2.Enabled := True;
    except
      MessageDlg('������ ������!', mtError, [mbOK], 0);
    end;
  end;
  stat1.Panels[1].Text := IntToStr(DataModule2.IBQuery2.RecordCount);
  updateDB;
end;

procedure TForm10.dtp1Click(Sender: TObject);
begin
  rb1.Checked := True;
  btn2.Enabled := False;
end;

procedure TForm10.dtp2Click(Sender: TObject);
begin
  rb1.Checked := True;
  btn2.Enabled := False;
end;

procedure TForm10.btn2Click(Sender: TObject);
var itemsNum: Integer;
begin
  if TryStrToInt(InputBox('���� ������', '������� ����� ������:', ''), itemsNum) then
    Form7.edt1.Text := IntToStr(itemsNum)
  else
  begin
    MessageDlg('����� ������ ������ � �������', mtError, [mbOK], 0);
    Exit;
  end;  
  DataModule2.frReport1.LoadFromFile(repReg);
  DataModule2.frReport1.ShowReport;  
end;

procedure TForm10.edt3Enter(Sender: TObject);
begin
  rb4.Checked := True;
  btn2.Enabled := False;
end;

procedure TForm10.edt2Enter(Sender: TObject);
begin
  rb2.Checked := True;
  btn2.Enabled := False;
end;

procedure TForm10.edt1Enter(Sender: TObject);
begin
  rb3.Checked := True;
  btn2.Enabled := False;
end;

procedure TForm10.N1Click(Sender: TObject);
var i, width: Integer;
begin
  width := 0;
  for i := 0 to dbgrd1.Columns.Count - 1 do
  begin
    width := width + dbgrd1.Columns[i].Width;
    if Mouse.X < width then
      Break;
  end;

  PutInClipboard(VarToStr(DataModule2.IBQuery2.FieldValues[dbgrd1.Columns[i].FieldName]));
end;

procedure TForm10.dbgrd1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Mouse.X := X;
end;

end.
