unit ShowForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DataModule, StdCtrls, Menus, Vars;

type
  TForm6 = class(TForm)
    DBGrid1: TDBGrid;
    lbl1: TLabel;
    cbb1: TComboBox;
    pm1: TPopupMenu;
    N1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure DBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private

  public
    procedure updateDBGrid;
    { Public declarations }
  end;

var
  Form6: TForm6;
  Mouse: TPoint;

implementation

{$R *.dfm}

procedure TForm6.FormCreate(Sender: TObject);
begin
  DBGrid1.DataSource := DataModule2.ds2;
end;

procedure TForm6.updateDBGrid;
var clWidth: Integer; oneColWidth: Integer;
begin
  clWidth := DBGrid1.ClientWidth;
  oneColWidth := Round(clWidth / 20);

  DBGrid1.Columns.Items[0].Visible := False;
  DBGrid1.Columns.Items[1].Visible := False;
  DBGrid1.Columns.Items[2].Visible := False;
  DBGrid1.Columns.Items[3].Width := 2 * oneColWidth;
  DBGrid1.Columns.Items[4].Width := 1 * oneColWidth;
  DBGrid1.Columns.Items[5].Width := 3 * oneColWidth;
  DBGrid1.Columns.Items[6].Width := 2 * oneColWidth;
  DBGrid1.Columns.Items[7].Width := 3 * oneColWidth;
  DBGrid1.Columns.Items[8].Width := 3 * oneColWidth;
  DBGrid1.Columns.Items[9].Width := 3 * oneColWidth;
  DBGrid1.Columns.Items[10].Width := 3 * oneColWidth;

  {DBGrid1.Columns[1].Title.Caption := '��������';
  DBGrid1.Columns[2].Title.Caption := '���� ��������';
  DBGrid1.Columns[3].Title.Caption := '�������������';
  DBGrid1.Columns[4].Title.Caption := '������';
  DBGrid1.Columns[5].Title.Caption := '����� �������';
  DBGrid1.Columns[6].Title.Caption := '���� ������';
  }
end;

procedure TForm6.cbb1Change(Sender: TObject);
begin
  with DataModule2.IBQuery2 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM FINES WHERE REG_ID=' + VarToStr(DataModule2.IBQuery1.FieldValues['ID']));
    case cbb1.ItemIndex of
      0: SQL.Add(' ORDER BY POSIT');
      1: SQL.Add(' ORDER BY BARCODE');
      2: SQL.Add(' ORDER BY ADRESAT');
    end;
    Open;
  end;
  updateDBGrid;
end;

procedure TForm6.N1Click(Sender: TObject);
var i, width: Integer;
begin
  width := 0;
  for i := 0 to DBGrid1.Columns.Count - 1 do
  begin
    width := width + DBGrid1.Columns[i].Width;
    if Mouse.X < width then
      Break;
  end;

  PutInClipboard(VarToStr(DataModule2.IBQuery2.FieldValues[DBGrid1.Columns[i].FieldName]));
end;

procedure TForm6.DBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Mouse.X := X;
end;

end.
