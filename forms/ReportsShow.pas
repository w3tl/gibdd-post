unit ReportsShow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Vars, DataModule;

type
  TForm7 = class(TForm)
    rg1: TRadioGroup;
    btn1: TButton;
    rb1: TRadioButton;
    rb2: TRadioButton;
    rb3: TRadioButton;
    cbb1: TComboBox;
    cbb2: TComboBox;
    txt1: TStaticText;
    edt1: TEdit;
    txt2: TStaticText;
    procedure btn1Click(Sender: TObject);
    procedure showReport(report: Integer; registry: Integer);
    procedure cbb1Change(Sender: TObject);
    procedure rb1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

procedure TForm7.showReport(report: Integer; registry: Integer);
var file_path, sql_like, sql_order: string;
begin
  try
    sql_like := ''; // ������ �� ������
    case cbb2.ItemIndex of
      0: sql_order := ' ORDER BY ADRESAT'; // ���������� �� ��������
      1: sql_order := ' ORDER BY BARCODE'; // ���������� �� ���
      2: sql_order := ' ORDER BY POSIT';   // ���������� �� ������� (��� ����)
    end;
    case report of
      1:  file_path := repReg;   // ������
      2:  file_path := repBlank; // ��������� �������
      3:  file_path := repBack;  // �����������
    end;
    case registry of
      // ������ ������ � ����� �������� ->
      0: sql_like := ' AND (CITY LIKE ''�������������-����������%'')';     // -> �����
      1: sql_like := ' AND (CITY NOT LIKE ''�������������-����������%'')'; // -> �����������
      -1, 2: sql_like := '';                                                   // -> ���
      end;
    with DataModule2, DataModule2.IBQuery2 do
    begin
      Close;
      SQL.Text := 'SELECT * FROM FINES WHERE (REG_ID IN ' + GetIdString(reportList) + ')' + sql_like + sql_order;
      //ParamByName('id').Value := IBQuery1.FieldValues['ID'];
      Open;
      FetchAll;
      First;
      try
        frReport1.LoadFromFile(file_path);
      except
        ShowMessage('������� ������ ���� � �������� ����� (���� ��������� -> �����)');
      end;
      frReport1.ShowReport;
    end;
  except
    ShowMessage('������ �������� ������!');
  end;
end;

procedure TForm7.btn1Click(Sender: TObject);
var report, reg, val: Integer;
begin
  reg := -1;    // �� ��������� ��� ������� �� ������
  report := 1;  // �� ��������� ������ ������
  if rb1.Checked then // ������� ����� ������
  begin
    reg := cbb1.ItemIndex; // ����� || ����������� || ��� 
    if not TryStrToInt(edt1.Text, val) then // ������ � ���� "������" (�� �����)
    begin
      MessageDlg('�������� �������� ������ ������!', mtError, [mbOK], 0);
      edt1.SetFocus;
      Exit;
    end;  
  end;
  if rb2.Checked then report := 2; // ����� ��������� �������
  if rb3.Checked then report := 3; // ����� ������ �����������
  showReport(report, reg);
end;

procedure TForm7.cbb1Change(Sender: TObject);
var p, k: Integer;
begin
  rb1.Checked := True;
  try
    if cbb1.ItemIndex = 1 then
      k := 2
    else
      k := 1;
    p := VarToInt(DataModule2.IBQuery1.FieldValues['POSIT']);
    edt1.Text := IntToStr(2 * (p - 1) + k);
  except
    ShowMessage('�������� �������� ������!');
    edt1.Text := '1';
  end;
end;

procedure TForm7.rb1Click(Sender: TObject);
begin
  cbb1Change(Sender);
end;

end.
