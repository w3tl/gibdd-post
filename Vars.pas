unit Vars;

interface

uses IniFiles, Grids, ComObj, Classes, Variants, SysUtils, DataModule, Windows, Clipbrd;

const xlCellTypeLast=$000000B;
  xlExcel9795 = $0000002B;
  xlExcel8 = 56;
  xlShiftDown = -4121;
  xlFormatFromLeftOrAbove = 1;

var
  ExePath: string;
  IniPath: string;
  Ini: TIniFile;
    dbPath,
    dbLogin,
    dbPassword,
    dbServer,
    dbLCType: string;

    xlStartRow: Integer; // ��������� ������ � ������������� �����
    otStartNumber: Integer;
  repReg,
  repBlank,
  repBack,
  repExcelReg,
  repExcelLost: string;

  ImportGrid: TStringGrid;
  Excel, Range: Variant;

  tableFinesFields: TStrings;
  tableFinesPos: array of Integer;

  reportList: TStringList; // ��������� ������ � ������� ����

  regNumber: Integer; // ����� �������
  regDate: Integer;   // ������� � ����� �������������

  reportSort: Boolean;   // ���������� � ������
  reportDate: TDateTime; // ���� �������� ������

  incityReport: Boolean; // ���� ��� ��������� ������ (�����/��������)

  sqlSelectRegistries: string;
  function VarToInt(Value: Variant): Integer;
  procedure CloseExcel;
  function ChangeCity(city: string): string;
  function GetIdString(list: TStringList): string; // ���������� ������ ��� ��������� WHERE ID IN (string)
  procedure PutInClipboard(data: string);

implementation

function VarToInt(Value: Variant): Integer;
begin
  Result := StrToInt(VarToStr(Value));
end;

procedure CloseExcel;
begin
  Excel.Visible := True;
  Excel.ActiveWorkbook.Close;
  //Excel.Quit;
  Excel := Unassigned;
end;

function ChangeCity(city: string): string;
begin
  if Length(city) = 0 then Exit;
  if city[Length(city) - 1] = ' ' then
  city := Copy(city, 1, Length(city) - 2);
  with DataModule2.IBChangeQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT CITY_TO FROM CHANGE_CITY WHERE CITY_FROM=:from');
    ParamByName('from').AsString := city;
    Open;
    FetchAll;
    if RecordCount = 0 then
      Result := city
    else
    begin
      Result := VarToStr(FieldValues['CITY_TO']);
    end;  
  end;  
end;

function GetIdString(list: TStringList): string;
var i: Integer;
begin
  Result := '(';
  for i := 0 to list.Count - 2 do
  begin
    Result := Result + list[i] + ', ';
  end;
  Result := Result + list[list.Count - 1] + ')';
end;

procedure PutInClipboard(data: string);
var CurrentHKL: HKL; 
begin
CurrentHKL:= GetKeyboardLayout(0);
ActivateKeyboardLayout($0419, KLF_ACTIVATE);
Clipboard.AsText:= data; 
ActivateKeyboardLayout(CurrentHKL, KLF_ACTIVATE); 
end;

end.
