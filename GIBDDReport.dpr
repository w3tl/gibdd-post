program GIBDDReport;

uses
  Forms,
  MainForm in 'MainForm.pas' {Form1},
  DataModule in 'forms\modules\DataModule.pas' {DataModule2: TDataModule},
  Vars in 'Vars.pas',
  ImportForm in 'forms\ImportForm.pas' {Form2},
  DBSettings in 'forms\settings\DBSettings.pas' {Form3},
  ReportSettings in 'forms\settings\ReportSettings.pas' {Form4},
  ImportSettings in 'forms\settings\ImportSettings.pas' {Form5},
  ShowForm in 'forms\ShowForm.pas' {Form6},
  ReportsShow in 'forms\ReportsShow.pas' {Form7},
  AutoChangeSettings in 'forms\settings\AutoChangeSettings.pas' {Form8},
  CompareForm in 'forms\CompareForm.pas' {Form9},
  SearchForm in 'forms\SearchForm.pas' {Form10},
  ReportDialog in 'forms\ReportDialog.pas' {Form11},
  ChangeForm in 'forms\ChangeForm.pas' {Form12},
  ShowImport in 'forms\ShowImport.pas' {Form13},
  ReportListForm in 'forms\ReportListForm.pas' {Form14};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := '������ � ���������������';
  Application.CreateForm(TDataModule2, DataModule2);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TForm10, Form10);
  Application.CreateForm(TForm11, Form11);
  Application.CreateForm(TForm12, Form12);
  Application.CreateForm(TForm13, Form13);
  Application.CreateForm(TForm14, Form14);
  Application.Run;
end.
