unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, XPMan, Grids, DBGrids, Vars, IniFiles, DataModule, ImportForm,
  DBSettings, ReportSettings, ImportSettings, StdCtrls, ComObj, DB, CompareForm,
  DateUtils, ReportDialog, ChangeForm, ComCtrls, Buttons, AutoChangeSettings, ReportListForm,
  ExtCtrls, FR_Desgn, FR_E_HTM, FR_E_CSV, FR_E_RTF, FR_E_TXT, FR_RRect, FR_Chart,
  FR_BarC, FR_Shape, FR_ChBox, FR_Rich, FR_OLE, FR_DSet, FR_DBSet, FR_Class;

type
  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    MainMenu1: TMainMenu;
    XPManifest1: TXPManifest;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N17: TMenuItem;
    btn1: TBitBtn;
    btn3: TBitBtn;
    btn5: TBitBtn;
    N21: TMenuItem;
    dlgSave1: TSaveDialog;
    N22: TMenuItem;
    N23: TMenuItem;
    pb1: TProgressBar;
    btn6: TBitBtn;
    lbl1: TLabel;
    cbb1: TComboBox;
    dtp1: TDateTimePicker;
    dtp2: TDateTimePicker;
    btn7: TButton;
    btn8: TBitBtn;
    N14: TMenuItem;
    N15: TMenuItem;
    lbl4: TLabel;
    btn9: TButton;
    grp1: TGroupBox;
    lbl5: TLabel;
    lbl2: TLabel;
    lbl3: TLabel;
    edt1: TEdit;
    btn2: TButton;
    btn4: TButton;
    btn10: TButton;
    procedure FormCreate(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbb1Change(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure btn8Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure DBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure N23Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn10Click(Sender: TObject);
  private
    procedure updateDBGrid;
    procedure frGetValue(const ParName: String; var ParValue: Variant);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Mouse: TPoint;

implementation

uses SearchForm, ShowForm, ReportsShow;

{$R *.dfm}

procedure ReadSettings();
begin
  ExePath := ExtractFilePath(Application.ExeName);
  IniPath := ExePath + '\settings.ini';
  Ini := TIniFile.Create(IniPath);
  dbPath := Ini.ReadString('Database', 'path', ExePath + 'db\GIBDD.fdb');
  dbLogin := Ini.ReadString('Database', 'login', 'SYSDBA');
  dbPassword := Ini.ReadString('Database', 'password', 'masterkey');
  dbServer := Ini.ReadString('Database', 'server', 'localhost');
  dbLCType := Ini.ReadString('Database', 'lctype', 'WIN1251');
  xlStartRow := Ini.ReadInteger('Import', 'StartRow', 2);
  regNumber := Ini.ReadInteger('Import', 'regNumber', 1);
  regDate := Ini.ReadInteger('Import', 'regDate', 1);

  repReg := Ini.ReadString('Reports', 'reg', ExePath + 'reports\f103.frf');
  repBlank := Ini.ReadString('Reports', 'blank', ExePath + 'reports\blank.frf');
  repBack := Ini.ReadString('Reports', 'back', ExePath + 'reports\f119.frf');
  repExcelReg := Ini.ReadString('Reports', 'excelReg', ExePath + 'templates\report.xls');
  repExcelLost := Ini.ReadString('Reports', 'excelLost', ExePath + 'templates\lost.xls');
  otStartNumber := Ini.ReadInteger('Report', 'StartNumber', 45);

  ImportGrid := TStringGrid.Create(nil);
  ImportGrid.RowCount := 0;

  reportList := TStringList.Create;
  reportList.Sorted := True;

  Ini.Free;
end;

procedure ConnectDatabase();
begin
  try
    with DataModule2.IBDatabase1 do
    begin
      DatabaseName := dbServer + ':' + dbPath;
      LoginPrompt := false;
      Params.Clear;
      Params.Add('USER_NAME=' + dbLogin);
      Params.Add('PASSWORD=' + dbPassword);
      Params.Add('LC_CTYPE=' + dbLCType);
      Connected := True;
    end;
  except on E: Exception do
    begin
      MessageDlg('��������� ������ ��� ����������� � ���� ������!' + #10#13 +
                 '���������, ��� ������ ������ ���� � ���� ������, ���� ������ ��������' +
                 E.Message, mtError, [mbOk], 0);
    end;
  end;

end;

procedure InitQuery();
begin
  with DataModule2.IBQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select * from REGISTRIES');
    Open;
  end;
  with DataModule2.IBQuery2 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select * from FINES');
    Open;
  end;
end;

procedure TForm1.frGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if ParName = 'dbcount' then
    ParValue := DataModule2.IBQuery2.RecordCount;
  if ParName = 'reportNumber' then
  begin
    ParValue := Form7.edt1.Text; // ����� ������ ���� � ����� ������ ������
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
var i: Integer;
begin
  DataModule2.frReport1.OnGetValue := frGetValue;

  Randomize;
  ReadSettings;
  ConnectDatabase;
  InitQuery;

  tableFinesFields := TStringList.Create;
  tableFinesFields.AddStrings(DataModule2.IBQuery2.FieldList);
  tableFinesFields.Delete(0);
  tableFinesFields.Delete(0);
  tableFinesFields.Delete(0);
  ImportGrid.ColCount := tableFinesFields.Count;
  SetLength(tableFinesPos, tableFinesFields.Count);

  Ini := TIniFile.Create(ExePath + '\settings.ini');
  for i := 0 to tableFinesFields.Count - 1 do
    tableFinesPos[i] := Ini.ReadInteger('Table', tableFinesFields[i], 1);
  Ini.Free;

  with DataModule2.IBQuery2 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select * from FINES where REG_ID=:id');
    Open;
  end;
  cbb1Change(Sender);

  dtp1.Date := Now;
  dtp2.Date := Now;
end;

procedure TForm1.N3Click(Sender: TObject);
var i: Integer;
begin
  with Form2 do
  begin
    for i := 0 to StringGrid1.RowCount - 1 do
      StringGrid1.Rows[i + 1].Clear;
      
    lbledt1.Text := '';
    lbledt2.Text := '';
    lbledt4.Text := '';
    dtp1.Date := Now;
    btn3.Enabled := False;
    btn4.Enabled := False;
    stat1.Panels[1].Text := '';
    stat1.Panels[3].Text := '';
    edtStartRow.Text := IntToStr(xlStartRow);
    edtNextNumber.Text := IntToStr(regNumber);
    edtResDate.Text := IntToStr(regDate);
    ShowModal;
  end;
  cbb1Change(Sender);
  DBGrid1.SetFocus;
end;

procedure TForm1.N5Click(Sender: TObject);
var id: Variant;
begin
  with DataModule2 do
  begin
    id := DataModule2.IBQuery1.FieldValues['ID'];
    with IBWorkQuery2 do
    begin
      Close;
      SQL.Clear;
      SQL.Add('delete from FINES where REG_ID=:id');
      ParamByName('id').Value := id;
      ExecSQL;
      Transaction.Commit;
      reportList.Delete(reportList.IndexOf(IntToStr(id)));
      lbl4.Caption := '�������: ' + IntToStr(reportList.Count);
    end;
    try
      if regNumber - 1 = VarToInt(IBQuery1.FieldValues['REG_NUMBER']) then
      begin
        regNumber := regNumber - 1;
        Ini := TIniFile.Create(IniPath);
        Ini.WriteInteger('Import', 'regNumber', regNumber);
        Ini.Free;
      end;
    except // ������, ���� ��� ������ �������
    end;
    with IBWorkQuery1 do
    begin
      Close;
      SQL.Clear;
      SQL.Add('delete from REGISTRIES where ID=:id');
      ParamByName('id').Value := id;
      ExecSQL;
      Transaction.Commit;
    end;
    IBQuery1.Open;
    cbb1Change(Sender);
      
    DBGrid1.SetFocus;
  end;  
end;

procedure TForm1.N13Click(Sender: TObject);
begin
  Form1.N5Click(Form1.N5);
end;

procedure TForm1.N8Click(Sender: TObject);
begin
  with Form3 do
  begin
    lbledt1.Text := dbLogin;
    lbledt2.Text := dbPassword;
    lbledt3.Text := dbLCType;
    lbledt4.Text := dbPath;
    lbledt5.Text := dbServer;
    ShowModal;
  end;  
end;

procedure TForm1.N9Click(Sender: TObject);
begin
  with Form4 do
  begin
    lbledt1.Text := repReg;
    lbledt2.Text := repBlank;
    lbledt3.Text := repBack;
    lbledt4.Text := repExcelReg;
    lbledt5.Text := repExcelLost;
    ShowModal;
  end;
end;

procedure TForm1.N10Click(Sender: TObject);
var i: Integer; field: string;
begin
  with Form5 do
  begin
    field := '';
    edt1.Text := IntToStr(xlStartRow);
    Edit1.Text := IntToStr(regNumber);
    edt2.Text := IntToStr(regDate);
    strngrd1.RowCount := tableFinesFields.Count;
    for i := 0 to tableFinesFields.Count - 1 do
    begin
      case i of
        0: field := '������'; 1: field := '���'; 2: field := '������';
        3: field := '�������������'; 4: field := '�����';
        5: field := '�����'; 6: field := '�������'; 7: field := '����';
      end;
      strngrd1.Cells[0, i] := field;
      strngrd1.Cells[1, i] := IntToStr(tableFinesPos[i]);
    end;
    strngrd1.ColWidths[0] := 140;
    strngrd1.ColWidths[1] := 42;
    Height := (Height - strngrd1.Height) +
              (strngrd1.RowCount * strngrd1.DefaultRowHeight) + 2 * strngrd1.RowCount + 1;
    strngrd1.Height := strngrd1.RowCount * strngrd1.DefaultRowHeight + 2 * strngrd1.RowCount + 1;
    ShowModal;
  end;
end;

procedure TForm1.N7Click(Sender: TObject);
begin
  Form1.Close;
end;

procedure TForm1.N17Click(Sender: TObject);
var res: TModalResult; pDate: TDateTime; id: Variant; cdate: TDateTime;
begin
  with Form12, DataModule2 do
  begin
    edt1.Text := VarToStr(IBQuery1.FieldValues['CAPTION']);
    edt2.Text := VarToStr(IBQuery1.FieldValues['REG_NUMBER']);
    dtp1.Date := VarToDateTime(IBQuery1.FieldValues['CREATE_DATE']);
    edt3.Text := VarToStr(IBQuery1.FieldValues['POSIT']);
    if not TryStrToDate(VarToStr(IBQuery1.FieldValues['POST_DATE']), pDate) then
      dtp2.Date := Now
    else
      dtp2.Date := pDate;
  end;
  res := Form12.ShowModal;
  if res <> mrOk then
    Exit;

  id := DataModule2.IBQuery1.FieldValues['ID'];
  cdate := VarToDateTime(DataModule2.IBQuery1.FieldValues['CREATE_DATE']);
  with DataModule2.IBWorkQuery1, Form12 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('UPDATE REGISTRIES SET REG_NUMBER=:reg ' +
                                  'WHERE CREATE_DATE=:cdate');
    ParamByName('cdate').AsDate := cdate;
    ParamByName('reg').AsInteger := StrToInt(edt2.Text);
    ExecSQL;
    Transaction.Commit;

    Close;
    SQL.Clear;
    SQL.Add('UPDATE REGISTRIES SET CAPTION=:caption,' +
                                  'CREATE_DATE=:create_date,' +
                                  'POST_DATE=:post_date, ' +
                                  'POSIT=:posit ' +
                                  'WHERE ID=:id');
    ParamByName('caption').AsString := Copy(edt1.Text, 0, 25);
    ParamByName('create_date').AsDate := dtp1.Date;
    ParamByName('post_date').AsDate := dtp2.Date;
    ParamByName('posit').AsInteger := StrToInt(edt3.Text);
    ParamByName('id').Value := id;
    ExecSQL;
    Transaction.Commit;
  end;
  with DataModule2.IBQuery1 do
  begin
    Close;
    SQL.Text := 'select * from REGISTRIES';
    Open;
  end;
  cbb1Change(Sender);
end;

procedure TForm1.btn1Click(Sender: TObject);
begin
  Form1.N3Click(Sender);
end;

procedure TForm1.btn6Click(Sender: TObject);
begin
  with Form9 do
  begin
    rb2.Checked := True;
    edt1.Text := IntToStr(xlStartRow);
    edt2.Text := IntToStr(3);
    lbledt1.Text := '';
    pb1.Position := 0;
  end;  
  Form9.ShowModal;
end;

procedure TForm1.N4Click(Sender: TObject);
begin
  with Form6, DataModule2 do
  begin
    Caption := '����� "' + VarToStr(IBQuery1.FieldValues['CAPTION']) +
               '" �� ' + VarToStr(IBQuery1.FieldValues['CREATE_DATE']);
    with IBQuery2 do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM FINES WHERE REG_ID=:id ORDER BY POSIT');
      ParamByName('id').Value := IBQuery1.FieldValues['ID'];
      ds2.Enabled := True;
      Open;
    end;
    updateDBGrid;
    ShowModal;
    cbb1.ItemIndex := 0;
  end;
  cbb1Change(Sender);
    
  DBGrid1.SetFocus;
end;

procedure TForm1.N11Click(Sender: TObject);
begin
  Form1.N4Click(Sender);
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  with Form7 do
  begin
    cbb1Change(Sender);
    ShowModal;
  end;
  DBGrid1.SetFocus;
end;

procedure TForm1.N18Click(Sender: TObject);
begin
  Form7.showReport(1, 0);
end;

procedure TForm1.DBGrid1DblClick(Sender: TObject);
var id: String; i: Integer;
begin
  id := VarToStr(DataModule2.IBQuery1.FieldValues['ID']);
  if reportList.Find(id, i) then begin
    reportList.Delete(i);
  end
  else begin
    reportList.Add(id);
  end;
  DBGrid1.Refresh;
  lbl4.Caption := '�������: ' + IntToStr(reportList.Count);
  //Form1.N4Click(Sender);
end;

procedure CloseExcel();
begin
  try
    Excel.Quit;
    Excel.DisplayAlerts := True;
  finally
    Excel := Unassigned;
  end;
end;

procedure TForm1.btn5Click(Sender: TObject);
var WorkSheet, WorkBook, Range, URange: Variant; _date: string;
i, row, num, posit, posits, open, CurRow, allcity, outcity, incity, allres: Integer;
ids: array of Integer; dialogResult: TModalResult; post_date: TDateTime;
post_all, sort: Boolean; FData: OleVariant;
positsVal: array of Integer; regs: array of string; regs_counts: TStringList;
begin
  if reportList.Count = 0 then begin
    ShowMessage('�������� ������� ��� �������� ������!');
    Exit;
  end;

  with DataModule2.IBWorkQuery1 do begin
    SQL.Text := 'SELECT REG_NUMBER, CREATE_DATE, CAPTION FROM REGISTRIES WHERE (ID IN ' + GetIdString(reportList) + ')';
    Open;
    FetchAll;
    First;
  end;
  Form14.strngrd1.RowCount := 2;
  for i := 1 to reportList.Count do begin
    Form14.strngrd1.Cells[0, i] := '1';
    if i > 1 then Form14.strngrd1.RowCount := Form14.strngrd1.RowCount + 1;
    Form14.strngrd1.Cells[1, i] := DataModule2.IBWorkQuery1.FieldValues['CREATE_DATE'];
    Form14.strngrd1.Cells[2, i] := DataModule2.IBWorkQuery1.FieldValues['REG_NUMBER'];
    Form14.strngrd1.Cells[3, i] := DataModule2.IBWorkQuery1.FieldValues['CAPTION'];
    DataModule2.IBWorkQuery1.Next;
  end;
  Form14.dtp1.Date := Date;

  dialogResult := Form14.ShowModal;
  if dialogResult <> mrOk then Exit;
  regs_counts := TStringList.Create;
  SetLength(regs, Form14.strngrd1.RowCount - 1);
  for i := 1 to Form14.strngrd1.RowCount - 1 do begin
    try
      num := StrToInt(Form14.strngrd1.Cells[0, i]) - 1;
      if length(regs[num]) <> 0 then regs[num] := regs[num] + ', ';
      regs[num] := regs[num] + reportList[i - 1];
      if regs_counts.IndexOf(Form14.strngrd1.Cells[0, i]) = -1 then
      regs_counts.Add(Form14.strngrd1.Cells[0, i]);
    except
      ShowMessage('������� �������� �������� � ���� "����"');
      Exit;
    end;
  end;
  post_date := Form14.dtp1.Date;
  sort := Form14.chk1.Checked;
  post_all := Form14.chk2.Checked;
  allres := 0;

  try
    try
      try
        Excel := GetActiveOleObject('Excel.Application');
      except
        Excel := CreateOleObject('Excel.Application');
      end;
      Excel.Visible := False;
      WorkBook := Excel.Workbooks.Open(repExcelReg, 0, True);
      Excel.DisplayAlerts := False;
    except
        raise Exception.Create('������ �������� �����!' + #10#13 +
                '���������, ��� ������ ���������� � ��� ��������� Excel �������.');
    end;

    posits := regs_counts.Count;

    for posit := 1 to posits - 1 do
      WorkBook.WorkSheets[posit].Copy(EmptyParam,
                                      WorkBook.WorkSheets[posit]);
    for posit := 1 to posits do
    begin
      with DataModule2.IBQuery2 do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT REG_ID, ADRESAT, KRAI, REGION, CITY, ADDRESS, BARCODE, RESOLUTION FROM FINES ' +
                'WHERE REG_ID IN (' + regs[StrToInt(regs_counts[posit - 1]) - 1] + ')');
        if sort then
          SQL.Add(' ORDER BY ADRESAT');
        Open;
        FetchAll;
        First;
      end;

      with DataModule2.IBWorkQuery1 do
      begin
        Close;
        SQL.Text := 'SELECT FIRST 1 CREATE_DATE, REG_NUMBER FROM REGISTRIES WHERE ID=:id';
        ParamByName('id').Value := DataModule2.IBQuery2.FieldValues['REG_ID'];
        Open;
      end;  

      WorkSheet := WorkBook.WorkSheets[posit];
      WorkSheet.Range['A1'].Value :=
                       '������ � ' +
              VarToStr(DataModule2.IBWorkQuery1.FieldValues['REG_NUMBER']) +
                       WorkSheet.Range['A1'].Value +
              VarToStr(DataModule2.IBWorkQuery1.FieldValues['CREATE_DATE']);

      WorkSheet.Range['A7'].Value := WorkSheet.Range['A7'].Value +
                                     IntToStr(DataModule2.IBQuery2.RecordCount);
      pb1.Max := DataModule2.IBQuery2.RecordCount;
      pb1.Position := 0;
      pb1.Visible := True;
      allres := allres + pb1.Max;
      row := 1;
      FData := VarArrayCreate([1, DataModule2.IBQuery2.RecordCount, 1, 6], varVariant);

      while not DataModule2.IBQuery2.Eof do
      begin
        with DataModule2.IBQuery2 do
        begin
          FData[row, 1] := row;
          FData[row, 2] := FieldValues['ADRESAT'];
          FData[row, 3] := //FieldValues['KRAI'] + ', ' +
                           //FieldValues['REGION'] + ', ' +
                           FieldValues['CITY'];
          FData[row, 4] := FieldValues['ADDRESS'];
          FData[row, 5] := FieldValues['BARCODE'];
          FData[row, 6] := FieldValues['RESOLUTION'];
          
          row := row + 1;
          Next;
        end;
        pb1.Position := row - 2;
      end;

      Range := WorkSheet.Range['A10:F' + IntToStr(DataModule2.IBQuery2.RecordCount + 8)];
      Range.EntireRow.Insert(xlShiftDown, xlFormatFromLeftOrAbove);
      Range := WorkSheet.Range['A10:F' + IntToStr(DataModule2.IBQuery2.RecordCount + 9)];
      Range.Value := FData;
      VarClear(FData);
    end;

    WorkSheet := WorkBook.WorkSheets[WorkBook.WorkSheets.Count];
    Range := WorkSheet.Range['A10:I10'];
    for posit := 1 to posits do
    begin
      with DataModule2.IBQuery2 do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT REG_ID FROM FINES WHERE REG_ID IN (' + regs[StrToInt(regs_counts[posit - 1]) - 1] + ')');
        Open;
        FetchAll;
        allcity := RecordCount;
        Close;
        SQL.Clear;
        SQL.Add('SELECT REG_ID, CITY FROM FINES ' +
                'WHERE REG_ID IN (' + regs[StrToInt(regs_counts[posit - 1]) - 1] + ') ' +
                'AND CITY LIKE ''�������������-����������%''');
        Open;
        FetchAll;
        incity := RecordCount;
        outcity := allcity - incity;
      end;
      if posit < regs_counts.Count then
      begin
        Range.EntireRow.Insert(xlShiftDown, xlFormatFromLeftOrAbove);
        Range.EntireRow.Insert(xlShiftDown, xlFormatFromLeftOrAbove);
      end
      else
        Range.EntireRow.Insert(xlShiftDown, xlFormatFromLeftOrAbove);

      URange := WorkSheet.Range['A' + IntTostr(2 * (posit - 1) + 10) + ':E' + IntTostr(2 * (posit - 1) + 10)];
      URange.Columns[1].Value := IntToStr(2 * posit - 1);
      URange.Columns[2].Value := '���������� �������� �����������';
      URange.Columns[4].Value := '��';
      URange.Columns[5].Value := IntToStr(incity);
      URange := WorkSheet.Range['A' + IntTostr(2 * (posit - 1) + 11) + ':E' + IntTostr(2 * (posit - 1) + 11)];
      URange.Columns[1].Value := IntToStr(2 * posit);
      URange.Columns[2].Value := '���������� �������� �����������';
      URange.Columns[4].Value := '��';
      URange.Columns[5].Value := IntToStr(outcity);
    end;
    // ���� �������� �� ����� �� ��������� �������� �������
    WorkSheet.Range['A' + IntTostr(2 * posits + 12)].Value := DateToStr(post_date);
    // ���-�� ���� �������������
    WorkSheet.Range['E' + IntTostr(2 * posits + 14)].Value := IntToStr(allres);

    dlgSave1.FileName := IntToStr(DataModule2.IBQuery1.FieldValues['REG_NUMBER']) +
                         ' ' + DateToStr(post_date) + ' ' + IntToStr(allres);
    if dlgSave1.Execute then
    begin
      WorkBook.SaveAs(dlgSave1.FileName, xlExcel8);
      WorkBook.Activate;
    end else
    begin
      WorkBook.Close;
    end;  
    pb1.Visible := False;
    with DataModule2 do // ������������ ���� �������� �� ����� ��� ���� � ����� �� �����
    begin
      with IBWorkQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Add('UPDATE REGISTRIES SET POST_DATE=:pdate WHERE ID IN ' + GetIdString(reportList));
        if post_all then
          SQL.Add(' AND POST_DATE is NULL');
        ParamByName('pdate').AsDate := post_date;
        ExecSQL;
      end;
      with IBQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Add('SELECT * FROM REGISTRIES');
        Open;
      end;
      cbb1Change(Sender);
    end;
  except on E: Exception do
    begin
      MessageDlg(E.Message, mtError, [mbOk], 0);
      CloseExcel;
      pb1.Visible := False;
      SetLength(positsVal, 0);
      Exit;
    end;
  end;
  SetLength(positsVal, 0);
  Excel.Visible := True;
end;

procedure TForm1.N21Click(Sender: TObject);
begin
  Form1.btn5Click(Sender);
end;

procedure TForm1.updateDBGrid;
var clWidth: Integer; oneColWidth: Integer;
begin
  clWidth := DBGrid1.ClientWidth;
  oneColWidth := Round(clWidth / 17);

  DBGrid1.Columns.Items[0].Visible := False;
  DBGrid1.Columns.Items[1].Width := 6 * oneColWidth;
  DBGrid1.Columns.Items[2].Width := 3 * oneColWidth;
  DBGrid1.Columns.Items[3].Width := 2 * oneColWidth;
  DBGrid1.Columns.Items[4].Width := 2 * oneColWidth;
  DBGrid1.Columns.Items[5].Width := 2 * oneColWidth;
  DBGrid1.Columns.Items[6].Width := 3 * oneColWidth;

  DBGrid1.Columns[1].Title.Caption := '��������';
  DBGrid1.Columns[2].Title.Caption := '���� ��������';
  DBGrid1.Columns[3].Title.Caption := '�������������';
  DBGrid1.Columns[4].Title.Caption := '������';
  DBGrid1.Columns[5].Title.Caption := '����� �������';
  DBGrid1.Columns[6].Title.Caption := '���� ������';
end;

procedure TForm1.FormShow(Sender: TObject);
begin  
  DBGrid1.SetFocus;
end;

procedure TForm1.cbb1Change(Sender: TObject);
begin
  DataModule2.IBQuery1.Close;
  DataModule2.IBQuery1.SQL.Clear;
  DataModule2.IBQuery1.SQL.Add('SELECT * FROM REGISTRIES WHERE CREATE_DATE>:cd ORDER BY CREATE_DATE DESC');
  case cbb1.ItemIndex of
    0: DataModule2.IBQuery1.ParamByName('cd').AsDate := EncodeDate(2000, 01, 01);
    1: DataModule2.IBQuery1.ParamByName('cd').AsDate := StrToDate('01.01.' + IntToStr(YearOf(Now)));
    2: DataModule2.IBQuery1.ParamByName('cd').AsDate := IncMonth(Now, -6);
    3: DataModule2.IBQuery1.ParamByName('cd').AsDate := IncMonth(Now, -1);
    4: DataModule2.IBQuery1.ParamByName('cd').AsDate := IncDay(Now, -1);
  end;
  DataModule2.IBQuery1.Open;
  updateDBGrid;
end;

procedure TForm1.btn7Click(Sender: TObject);
begin
  with DataModule2.IBQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select * from REGISTRIES WHERE CREATE_DATE>=:min_date AND CREATE_DATE<=:max_date');
    ParamByName('min_date').AsDate := dtp1.Date;
    ParamByName('max_date').AsDate := dtp2.Date;
    Open;
  end;
  cbb1.ItemIndex := 0;
  updateDBGrid;
end;

procedure TForm1.btn8Click(Sender: TObject);
var res: TModalResult;
begin
  with Form10 do
  begin
    btn2.Enabled := False;
    res := ShowModal;
  end;
  DataModule2.IBQuery1.Close;
  DataModule2.IBQuery1.SQL.Clear;
  DataModule2.IBQuery1.SQL.Add('SELECT * FROM REGISTRIES');
  DataModule2.IBQuery1.Open;
  cbb1Change(Sender);
end;

procedure TForm1.N12Click(Sender: TObject);
begin
  with Form7 do
  begin
    ShowModal;
  end;
  DBGrid1.SetFocus;
end;

procedure TForm1.N14Click(Sender: TObject);
var colWidth, i: Integer;
begin
  with DataModule2.IBChangeQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Add('SELECT * FROM CHANGE_CITY');
    Open;
  end;
  with Form8 do
  begin
    colWidth := Round(DBGrid1.ClientWidth / (DBGrid1.Columns.Count - 1)) - 1;
    for i := 0 to DBGrid1.Columns.Count - 1 do
      case i of
        0: DBGrid1.Columns[i].Visible := False;
      else
        DBGrid1.Columns[i].Width := colWidth;
      end;
    ShowModal;
  end;
end;

procedure TForm1.N15Click(Sender: TObject);
var i, width: Integer;
begin
  width := 0;
  for i := 0 to DBGrid1.Columns.Count - 1 do
  begin
    width := width + DBGrid1.Columns[i].Width;
    if Mouse.X < width then
      Break;
  end;
  PutInClipboard(VarToStr(DataModule2.IBQuery1.FieldValues[DBGrid1.Columns[i].FieldName]));
end;

procedure TForm1.DBGrid1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Mouse.X := X;
end;

procedure TForm1.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
  var id: string; i: Integer;
begin
  id := VarToStr(Column.Field.DataSet.FieldValues['ID']);
  IF reportList.Find(id, i)
	then begin
	with  DBGrid1.Canvas do 
	begin
    if  gdSelected in State then begin
      Brush.Color := clGray;
      Font.Color := clHighlightText;
    end else begin
		  Brush.Color := clGreen;
		  Font.Color := clWhite;
    end;
		FillRect(Rect);
		TextOut(Rect.Left+2,Rect.Top+2,Column.Field.Text);
	end;
	end;
end;

procedure TForm1.N23Click(Sender: TObject);
begin
  MessageDlg('�� "������ � ��������������� �����"' + #10#13 + #10#13 +
             #169 + ' 2014 ���������� ������� <mail@dostovalov.tk>', mtInformation, [mbOK], 0);
end;

procedure TForm1.btn9Click(Sender: TObject);
begin
  reportList.Clear;
  lbl4.Caption := '�������: 0';
  DBGrid1.Refresh;
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  with DataModule2.IBQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select * from REGISTRIES WHERE REG_NUMBER=:num');
    ParamByName('num').AsInteger := StrToInt(edt1.Text);
    Open;
  end;
  cbb1.ItemIndex := 0;
  updateDBGrid;
end;

procedure TForm1.btn4Click(Sender: TObject);
begin
  edt1.Text := '';
  DataModule2.IBQuery1.Close;
  DataModule2.IBQuery1.SQL.Clear;
  DataModule2.IBQuery1.SQL.Add('SELECT * FROM REGISTRIES WHERE CREATE_DATE>:cd ORDER BY CREATE_DATE DESC');
  case cbb1.ItemIndex of
    0: DataModule2.IBQuery1.ParamByName('cd').AsDate := EncodeDate(2000, 01, 01);
    1: DataModule2.IBQuery1.ParamByName('cd').AsDate := StrToDate('01.01.' + IntToStr(YearOf(Now)));
    2: DataModule2.IBQuery1.ParamByName('cd').AsDate := IncMonth(Now, -6);
    3: DataModule2.IBQuery1.ParamByName('cd').AsDate := IncMonth(Now, -1);
    4: DataModule2.IBQuery1.ParamByName('cd').AsDate := IncDay(Now, -1);
  end;
  DataModule2.IBQuery1.Open;
  updateDBGrid;
end;

procedure TForm1.btn10Click(Sender: TObject);
var ar: Variant;
begin
  ar := VarArrayCreate([1, 1], varVariant);

  ar[1] := VarArrayOf([1, 'sd']);
  ShowMessage(ar[1][1]);

end;

end.
